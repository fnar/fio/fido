﻿using System.IO;
using System.Reflection;

using Microsoft.EntityFrameworkCore;

using fido.Database.Models;

namespace fido.Database
{
	public class FIDODbContext : DbContext
	{
		public DbSet<DefaultGroupIdModel> DefaultGroupIds { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder options)
		{
			options.UseSqlite($"Data Source=fido.db").UseLazyLoadingProxies();
		}
	}
}
