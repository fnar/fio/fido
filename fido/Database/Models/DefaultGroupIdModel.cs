﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace fido.Database.Models
{
	public class DefaultGroupIdModel
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public ulong DiscordChannelId { get; set; }

		public ulong GroupId { get; set; }
	}
}
