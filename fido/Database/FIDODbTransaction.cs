﻿using System;

using Microsoft.EntityFrameworkCore.Storage;

namespace fido.Database
{
    public class FIDODbTransaction : IDisposable
    {
        public FIDODbContext DB
        {
            get; private set;
        } = null;

        private IDbContextTransaction _transaction = null;

        public FIDODbTransaction()
        {
            DB = new FIDODbContext();
            _transaction = DB.Database.BeginTransaction();
        }

        public void Dispose()
        {
            _transaction?.Commit();
            _transaction?.Dispose();
            _transaction = null;

            DB?.Dispose();
            DB = null;
        }
    }
}
