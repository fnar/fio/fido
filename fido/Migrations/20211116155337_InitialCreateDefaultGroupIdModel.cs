﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace fido.Migrations
{
    public partial class InitialCreateDefaultGroupIdModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DefaultGroupIds",
                columns: table => new
                {
                    DiscordChannelId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    GroupId = table.Column<ulong>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DefaultGroupIds", x => x.DiscordChannelId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DefaultGroupIds");
        }
    }
}
