﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using FIOWeb.JsonPayloads;
using Fido.Data;

using Serilog;

namespace Fido.commands
{
    public class MaterialCommands : BaseCommandModule
    {
        public MaterialCommands()
        {
        }

        private static string SnakeCaseToString(string input)
        {
            return input.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => char.ToUpperInvariant(s[0]) + s.Substring(1, s.Length - 1))
                .Aggregate(new StringBuilder(), (r1, r2) => r1.Append(" " + r2.ToString()), (r1) => r1.Remove(0, 1).ToString());
        }

        [Command("cx")]
        [DescriptionAttribute("Get CX data for a particular material.\nNOTE: This command may take a little while to get data.\nExamples: `cx h2o.ci1`, `cx bse`")]
        public async Task CXCommand(CommandContext ctx,
            [DescriptionAttribute("Ticker (with/without CX identifier) of material to lookup.")]
            string material)
        {
            // Might take a while.  Let the user know we're thinking.
            await ctx.TriggerTypingAsync();
            var response = new DiscordEmbedBuilder();
            MaterialPayload item = null;
            Log.Debug($"{material}");
            Regex rgx = new Regex(@"(?<ticker>\w+)\.?(?<cx>\w+)?");
            Match matches = rgx.Match(material);
            if(matches.Groups["ticker"] != null)
            {
                item = await Materials.FindByTicker(matches.Groups["ticker"].Value);
            }
            if(item == null)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: no match", "No matching materials were found.");
                await ctx.RespondAsync(response);
                return;
            }
            Log.Debug($"{item.Ticker} {item.Name}");
            List<ExchangePayload> cxdata = new List<ExchangePayload> { };
            if (matches.Groups["cx"] == null || matches.Groups["cx"].Length == 0)
            {
                Log.Debug("Fetching all CXes");
                // get all CX results
                cxdata = await Exchanges.GetExchangeList();
            }
            else
            {
                // get a specific CX result.
                Log.Debug($"Getting specific CX: {matches.Groups["cx"].Value}");
                ExchangePayload ep = await Exchanges.FindByCode(matches.Groups["cx"].Value);
                if(ep != null)
                {
                    cxdata = new List<ExchangePayload> { ep };
                }
            }
            Log.Debug($"List of CXes? {cxdata.Count}");
            if(cxdata == null || cxdata.Count == 0)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: no CX match", "Found material, but no matching CX.");
                await ctx.RespondAsync(response);
                return;
            }

            foreach(ExchangePayload cx in cxdata)
            {
                string cxshortname = cx.ExchangeName.Substring(0, cx.ExchangeName.IndexOf(" Commodity Exchange"));
                Log.Debug($"Lookup: {item.Ticker}.{cx.ExchangeCode} ({cxshortname})");
                ExchangeOrderPayload order = await Exchanges.GetExchangeOrder($"{item.Ticker}.{cx.ExchangeCode}");
                string orderdata = "```\n";
                orderdata += $"Ask:     {order.AskCount} @ {order.Ask:N2} {order.Currency}\n";
                orderdata += $"AvgPrice:{order.PriceAverage:N2} {order.Currency}\n";
                orderdata += $"Bid:     {order.BidCount} @ {order.Bid:N2} {order.Currency}\n";
                orderdata += $"Supply:  {order.Supply} {(order.MMSell == null ? "" : $"(∞ @ {order.MMSell})")}\n";
                orderdata += $"Demand:  {order.Demand} {(order.MMBuy  == null ? "" : $"(∞ @ {order.MMBuy})")}\n";
                if(cxdata.Count == 1)
                {
                    // display additional data
                    orderdata += $"\n";
                    orderdata += $"AllTime: {order.AllTimeLow:N2} - {order.AllTimeHigh:N2}\n";
                    orderdata += $"{order.Traded} traded for {order.VolumeAmount:N2} {order.Currency}\n";
                    orderdata += $"$ Band:  {order.NarrowPriceBandLow:N2} - {order.NarrowPriceBandHigh:N2}\n";
                }
                orderdata += "```";
                response.AddField($"{item.Ticker}.{cx.ExchangeCode} ({cxshortname})", orderdata);
            }

            var color = Materials.GetMaterialColor(item);
            response.WithColor(new DiscordColor(color.r, color.g, color.b));

            await ctx.RespondAsync(response);
        }

        [Command("mat"), Aliases("material")]
        [DescriptionAttribute("Get information on a specific material")]
        public async Task MatCommand(CommandContext ctx,
            [DescriptionAttribute("Name or ticker(identifier) of the material to lookup")]
            string material)
        {
            var response = new DiscordEmbedBuilder();
            MaterialPayload item = await Materials.FindByTicker(material);
            if (item == null)
            {
                List<MaterialPayload> list = await Materials.FindByName(material);
                if (list.Count > 1)
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: multiple results.", "Multiple material results found.  Please be more specific.");
                    string lresponse = "```\n";
                    foreach (MaterialPayload entry in list)
                    {
                        lresponse += $"{entry.Ticker, -4} : {entry.Name}\n";
                    }
                    lresponse += "```";
                    response.AddField("Matches", lresponse);
                    await ctx.RespondAsync(response);
                    return;
                }
                if (list.Count == 0)
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: no match", "No matching materials were found.");
                    await ctx.RespondAsync(response);
                    return;
                }
                item = list[0];
            }
            
            response.AddField(item.Ticker, item.Name);
            if (item.CategoryName != null)
            {
                response.AddField("Category", SnakeCaseToString(item.CategoryName.ToLower()), true);
            }
            response.AddField("Weight", $"{item.Weight} t", true);
            response.AddField("Volume", $"{item.Volume} m³", true);
            var color = Materials.GetMaterialColor(item);
            response.WithColor(new DiscordColor(color.r, color.g, color.b));

            var usedByBuildings = (await Buildings.FindByInput(material))
                .Select(b => b.Ticker)
                .ToList();

            var producedByBuildings = (await Buildings.FindByOutput(material))
                .Select(b => b.Ticker)
                .ToList();

            if (usedByBuildings.Any())
            {
                response.AddField("Used By", string.Join(", ", usedByBuildings));
            }
            
            if (producedByBuildings.Any())
            {
                response.AddField("Produced By", string.Join(", ", producedByBuildings));
            }

            //response += $"> NaturalRes: {item.NaturalResource}\n"; // not avail?
            // production
            /*
            // So, there's a limit of 1024 characters to an Embed's Field's value.
            // Recipe data quickly exceeds that.  (H2O was ~1300 for just the outputs).
            // TODO: Determine how this can best be done, so we can restore the recipe data.

            List<RecipePayload> out_recipes = await Recipes.FindByOutputTicker(item.Ticker);
            if (out_recipes.Count() > 0)
            {
                string recipestr = "```\n";
                foreach (RecipePayload recipe in out_recipes)
                {
                    string inputs = "", outputs = "";
                    if (recipe.Inputs.Count > 0)
                    {
                        inputs = recipe.Inputs.Select(r => $"{r.Amount} {r.Ticker}")
                            .Aggregate(new StringBuilder(), (r1, r2) => r1.Append(" + " + r2.ToString()), (r1) => r1.Remove(0, 3).ToString());
                    }
                    if (recipe.Outputs.Count > 0)
                    {
                        outputs = recipe.Outputs.Select(r => $"{r.Amount} {r.Ticker}")
                            .Aggregate(new StringBuilder(), (r1, r2) => r1.Append(" + " + r2.ToString()), (r1) => r1.Remove(0, 3).ToString());
                    }
                    if (inputs.Length > 0 && outputs.Length > 0)
                    {
                        recipestr += $" {inputs} > ({Recipes.TimeString(recipe.TimeMs)} @ {recipe.BuildingTicker}) => {outputs}\n";
                    }
                }
                recipestr += "```";
                response.AddField("Production", recipestr);
            }

            // used-in (wrought)
            List<RecipePayload> in_recipes = await Recipes.FindByInputTicker(item.Ticker);
            if(in_recipes.Count() > 0)
            {
                string recipestr = "```\n";
                foreach (RecipePayload recipe in in_recipes)
                {
                    string inputs = "", outputs = "";
                    if (recipe.Inputs.Count > 0)
                    {
                        inputs = recipe.Inputs.Select(r => $"{r.Amount} {r.Ticker}")
                            .Aggregate(new StringBuilder(), (r1, r2) => r1.Append(" + " + r2.ToString()), (r1) => r1.Remove(0, 3).ToString());
                    }
                    if (recipe.Outputs.Count > 0)
                    {
                        outputs = recipe.Outputs.Select(r => $"{r.Amount} {r.Ticker}")
                            .Aggregate(new StringBuilder(), (r1, r2) => r1.Append(" + " + r2.ToString()), (r1) => r1.Remove(0, 3).ToString());
                    }
                    if (inputs.Length > 0 && outputs.Length > 0)
                    {
                        recipestr += $" {inputs} > ({Recipes.TimeString(recipe.TimeMs)} @ {recipe.BuildingTicker}) => {outputs}\n";
                    }
                }
                recipestr += "```";

                response.AddField("Used in", recipestr);
            }
            */

            await ctx.RespondAsync(response);
        }

    }
}
