﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using System.Threading.Tasks;
using System;
using FIOWeb.JsonPayloads;
using Fido.Data;
using Serilog;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSharpPlus.Entities;

namespace Fido.commands
{
    public class BuildingCommands : BaseCommandModule
    {
        private static void IncrementDictBy(SortedDictionary<string, int> parts, string key, int increment)
        {
            if (parts.ContainsKey(key))
            {
                parts[key] = parts[key] + increment;
            }
            else
            {
                parts[key] = increment;
            }
        }
        private static string SnakeCaseToString(string input)
        {
            return input.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => char.ToUpperInvariant(s[0]) + s.Substring(1, s.Length - 1))
                .Aggregate(new StringBuilder(), (r1, r2) => r1.Append(" " + r2.ToString()), (r1) => r1.Remove(0, 1).ToString());
        }

        [Command("bui"), Aliases("building")]
        [DescriptionAttribute("Get information on a specific building")]
        public async Task BuiCommand(CommandContext ctx,
            [DescriptionAttribute("Name or ticker(identifier) of the building to lookup")]
            string building)
        {
            BuildingPayload bp = await Buildings.FindByTicker(building);
            var response = new DiscordEmbedBuilder();

            if (bp == null)
            {
                List<BuildingPayload> bplist = await Buildings.FindByName(building);
                if (bplist.Count > 1)
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: multiple found", "Multiple buildings were found.  Please be more specific.");
                    string lresponse = "```\n";
                    foreach (BuildingPayload entry in bplist)
                    {
                        lresponse += $"{entry.Ticker,-4} : {entry.Name}\n";
                    }
                    lresponse += "```";
                    response.AddField("Matches", lresponse);
                    await ctx.RespondAsync(response);
                    return;
                }
                if (bplist.Count == 0)
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: no match", "No matching buildings were found.");
                    await ctx.RespondAsync(response);
                    return;
                }
                bp = bplist[0];
            }
            response.WithColor(new DiscordColor(79, 138, 16));

            Log.Debug($"found building: {bp.Ticker} {bp.Name}");
            response.AddField(bp.Ticker, bp.Name);
            if (bp.Expertise != null)
            {
                response.AddField("Expertise", SnakeCaseToString(bp.Expertise.ToLower()), true);
            }
            response.AddField("Area Required", bp.AreaCost.ToString());
            // material data
            string mat_list = "```\n";
            foreach (BuildingCost bc in bp.BuildingCosts)
            {
                mat_list += $"{bc.Amount} {bc.CommodityTicker} ({bc.CommodityName})\n";
            }
            if (!(bp.Name.Contains("planetaryProject") || bp.Name.Contains("corporationProject")))
            {
                mat_list += "(Plus planet-specific costs.)\n";
            }
            mat_list += "```";
            response.AddField("Materials Required", mat_list);

            // recipe data
            if (bp.Recipes.Count > 0)
            {
                // Some buildings only have 1 recipe (RIG) with no inputs or outputs.
                // Don't bother showing that pointless recipe.
                if (!(bp.Recipes.Count == 1 && bp.Recipes[0].Inputs.Count == 0 && bp.Recipes[0].Outputs.Count == 0))
                {
                    string recipe_list = "```\n";
                    foreach (Recipe recipe in bp.Recipes)
                    {
                        string inputs = "", outputs = "";
                        if (recipe.Inputs.Count > 0)
                        {
                            inputs = recipe.Inputs.Select(r => $"{r.Amount} {r.CommodityTicker}")
                                .Aggregate(new StringBuilder(), (r1, r2) => r1.Append(" + " + r2.ToString()), (r1) => r1.Remove(0, 3).ToString());
                        }
                        if (recipe.Outputs.Count > 0)
                        {
                            outputs = recipe.Outputs.Select(r => $"{r.Amount} {r.CommodityTicker}")
                                .Aggregate(new StringBuilder(), (r1, r2) => r1.Append(" + " + r2.ToString()), (r1) => r1.Remove(0, 3).ToString());
                        }
                        if (inputs.Length > 0 && outputs.Length > 0)
                        {
                            recipe_list += $"{inputs}, {Recipes.TimeString(recipe.DurationMs)} => {outputs}\n";
                        }
                    }
                    recipe_list += "```\n";
                    response.AddField("Recipes Available", recipe_list);
                }
            }
            if (bp.Pioneers + bp.Scientists + bp.Engineers + bp.Technicians + bp.Settlers > 0)
            {
                string wf_list = "```\n";
                if (bp.Pioneers > 0)
                    wf_list += $"{bp.Pioneers} Pioneers\n";
                if (bp.Settlers > 0)
                    wf_list += $"{bp.Settlers} Settlers\n";
                if (bp.Technicians > 0)
                    wf_list += $"{bp.Technicians} Technicians\n";
                if (bp.Engineers > 0)
                    wf_list += $"{bp.Engineers} Engineers\n";
                if (bp.Scientists > 0)
                    wf_list += $"{bp.Scientists} Scientists\n";
                wf_list += "```\n";
                response.AddField("Workforces", wf_list);
            }
            Log.Debug($"Response length: {response.ToString().Length}");
            await ctx.RespondAsync(response);
        }


        [Command("build")]
        [DescriptionAttribute("Generate a list of materials needed to build the buildingList on the specified planet.")]
        public async Task BuildCommand(CommandContext ctx,
            [DescriptionAttribute("Name or identifier of the planet you wish to build on.")]
            string planet,
            [RemainingText]
            [DescriptionAttribute("List of count & buildings, ex: `2 rig, 1 HB1`")]
            string buildingList)
        {
            var response = new DiscordEmbedBuilder();
            response.WithColor(new DiscordColor(79, 138, 16));

            SortedDictionary<string, int> buildings = new SortedDictionary<string, int>();
            SortedDictionary<string, int> parts = new SortedDictionary<string, int>();
            SortedDictionary<string, int> workforces = new SortedDictionary<string, int>();
            int areaRequired = 0;

            // Load the related planet.
            PlanetDataPayload planetData = await Planets.Find(planet);
            Log.Debug("Got result from planet list.");
            if (planetData == null)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: no match", $"Failed to find planet: {planet}.");
                await ctx.RespondAsync(response);
                return;
            }

            // scan through the building string, find some data
            Regex rgx = new Regex(@"((?<count>\d+) +(?<ticker>\w[\w\d]+))");
            MatchCollection matches = rgx.Matches(buildingList);
            foreach (Match match in matches)
            {
                GroupCollection group = match.Groups;
                int quantity = Int32.Parse(group["count"].Value);
                Log.Debug($"Matches: {quantity * 1}:{group["ticker"].Value}");
                BuildingPayload bp = await Buildings.FindByTicker(group["ticker"].Value);
                if (bp == null)
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: no match", $"Failed to find building: {group["ticker"].Value}");
                    await ctx.RespondAsync(response);
                    return;
                }
                IncrementDictBy(buildings, bp.Name, quantity);
                foreach (BuildingCost bc in bp.BuildingCosts)
                {
                    IncrementDictBy(parts, bc.CommodityTicker, bc.Amount * quantity);
                }
                // Add planetary-specific building parts
                List<BuildingCost> planetaryCosts = await Planets.PlanetBuildRequirements(planetData, bp);
                foreach (BuildingCost bc in planetaryCosts)
                {
                    IncrementDictBy(parts, bc.CommodityTicker, bc.Amount * quantity);
                }
                IncrementDictBy(workforces, "Pioneers", bp.Pioneers * quantity);
                IncrementDictBy(workforces, "Settlers", bp.Settlers * quantity);
                IncrementDictBy(workforces, "Technicians", bp.Technicians * quantity);
                IncrementDictBy(workforces, "Engineers", bp.Engineers * quantity);
                IncrementDictBy(workforces, "Scientists", bp.Scientists * quantity);
                areaRequired += bp.AreaCost * quantity;
            }

            // response to user.
            response.AddField("Planet", $"{planetData.PlanetName} ({planetData.PlanetNaturalId})");
            string blist = "```\n";
            foreach (KeyValuePair<string, int> building in buildings)
            {
                blist += $"{building.Value,4} {building.Key}\n";
            }
            blist += "```\n";
            response.AddField("Building List", blist, true);
            //      Do we show workforce?
            int workforcecount = 0;
            foreach (int wfv in workforces.Values)
            {
                workforcecount += wfv;
            }
            if (workforcecount > 0)
            {
                var workerListSb = new StringBuilder();
                workerListSb.AppendLine("```");
                foreach (KeyValuePair<string, int> wf in workforces)
                {
                    if (wf.Value > 0)
                    {
                        workerListSb.AppendLine($"{wf.Key}: {wf.Value}");
                    }
                }
                workerListSb.AppendLine("```");
                response.AddField("Workforce Req'd", workerListSb.ToString(), true);
            }

            response.AddField("Area Req'd", $"```\n{areaRequired}\n```", true);

            // Workforce needs
            if (workforcecount > 0)
            {
                var MaterialToDailyConsumption = new SortedDictionary<string, float>();
                foreach (var wf in workforces)
                {
                    string workforceName = wf.Key.Remove(wf.Key.Length - 1, 1);
                    var workforceNeeds = await Buildings.FindWorkforceNeedsByName(workforceName);
                    if (workforceNeeds != null)
                    {
                        foreach (var need in workforceNeeds.Needs)
                        {
                            if (wf.Value > 0)
                            {
                                float needAmount = (need.Amount / 100.0f) * wf.Value;
                                if (MaterialToDailyConsumption.ContainsKey(need.MaterialTicker))
                                {
                                    MaterialToDailyConsumption[need.MaterialTicker] += needAmount;
                                }
                                else
                                {
                                    MaterialToDailyConsumption.Add(need.MaterialTicker, needAmount);
                                }
                            }
                        }
                    }
                }

                var workerNeedsSb = new StringBuilder();
                workerNeedsSb.AppendLine("```");
                foreach (var kvp in MaterialToDailyConsumption)
                {
                    workerNeedsSb.AppendLine($"{kvp.Key}: {kvp.Value:N2}/d");
                }
                workerNeedsSb.AppendLine("```");
                response.AddField("Workforce Consumption", workerNeedsSb.ToString());
            }

            float totalPartsWeight = 0.0f;
            float totalPartsVolume = 0.0f;

            string partsDisplayList = "```\n";
            foreach (var partentry in parts)
            {
                partsDisplayList += $"{partentry.Key,-4} : {partentry.Value}\n";
                var matPartPayload = await Materials.FindByTicker(partentry.Key);
                totalPartsWeight += (matPartPayload.Weight * partentry.Value);
                totalPartsVolume += (matPartPayload.Volume * partentry.Value);
            }
            partsDisplayList += "```\n";

            response.AddField("Parts List", partsDisplayList);
            response.AddField("Parts Space", $"```\n{totalPartsWeight:N2}t\n{totalPartsVolume:N2}m³\n```");

            await ctx.RespondAsync(response);
        }

    }
}
