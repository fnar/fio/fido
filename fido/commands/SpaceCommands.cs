﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Fido.Data;

using org.mariuszgromada.math.mxparser;

namespace Fido.commands
{
    public class SpaceCommands : BaseCommandModule
    {
        private static List<string> WeightSuffixes = new List<string>
        {
            "t"
        };

        private static List<string> VolumeSuffixes = new List<string>
        {
            "m", "m3"
        };

        public static void GetWeightVolume(ref List<string> argumentList, ref float weight, ref float volume)
        {
            // Search for weightAvailable/volumeAvailable (determined by "t" and "m" suffix). These can only be the last two arguments
            var lastTwoArgs = argumentList.TakeLast(2).ToList();
            foreach (var arg in lastTwoArgs)
            {
                foreach (var WeightSuffix in WeightSuffixes)
                {
                    if (arg.ToLower().EndsWith(WeightSuffix))
                    {
                        string weightStr = arg.Remove(arg.Length - WeightSuffix.Length, WeightSuffix.Length);
                        if (float.TryParse(weightStr, out float parsedWeight))
                        {
                            weight = parsedWeight;
                            argumentList.RemoveAt(argumentList.FindLastIndex(x => x == arg));
                        }

                        break;
                    }
                }

                foreach (var VolumeSuffix in VolumeSuffixes)
                {
                    if (arg.ToLower().EndsWith(VolumeSuffix))
                    {
                        string volumeStr = arg.Remove(arg.Length - VolumeSuffix.Length, VolumeSuffix.Length);
                        if (float.TryParse(volumeStr, out float parsedVolume))
                        {
                            volume = parsedVolume;
                            argumentList.RemoveAt(argumentList.FindLastIndex(x => x == arg));
                        }

                        break;
                    }
                }
            }
        }

        [Command("fit")]
        [DescriptionAttribute("Get the maximum number of units that will fit in specified (optional) capacity.")]
        public async Task FitCommand(CommandContext ctx,
            [RemainingText]
            [DescriptionAttribute("Material followed by optional last two arguments specifying capacity\n `100t 100m`")]
            string arguments)
        {
            var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(79, 138, 16));

            // Replace all non-alphanumerics from the materialCountListStr with a space
            arguments = Regex.Replace(arguments, @"[^a-zA-Z0-9\.]", " ");

            // Split on space
            var argumentList = arguments.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            float weightAvailable = 500.0f;
            float volumeAvailable = 500.0f;
            GetWeightVolume(ref argumentList, ref weightAvailable, ref volumeAvailable);
            if (weightAvailable < 0.0001)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Weight Invalid", $"Weight of '{weightAvailable}' is not valid.");
                await ctx.RespondAsync(response);
                return;
            }
            if (volumeAvailable < 0.001)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Volume Invalid", $"Volume of '{volumeAvailable}' is not valid.");
                await ctx.RespondAsync(response);
                return;
            }

            if (argumentList.Count != 1)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Arguments", $"Invalid arguments specified.");
                await ctx.RespondAsync(response);
                return;
            }

            var ticker = argumentList[0];
            var matPayload = await Materials.FindByTicker(ticker);
            if (matPayload == null)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Ticker", $"Failed to find ticker '{ticker}'.");
                await ctx.RespondAsync(response);
                return;
            }

            int weightCount = (int)(weightAvailable / matPayload.Weight);
            int volumeCount = (int)(volumeAvailable / matPayload.Volume);
            int maxUnits = Math.Min(weightCount, volumeCount);

            double remainingWeight = weightAvailable - (matPayload.Weight * maxUnits);
            double remainingVolume = volumeAvailable - (matPayload.Volume * maxUnits);

            response.AddField("Max Units", $"{maxUnits} {matPayload.Ticker}");
            response.AddField("Remaining Weight", $"{remainingWeight:N2}t");
            response.AddField("Remaining Volume", $"{remainingVolume:N2}m³");
            await ctx.RespondAsync(response);
        }

        [Command("fitratio")]
        [DescriptionAttribute("Get the maximum number of a ratio of units that will fit in specified (optional) capacity.")]
        public async Task FitRatioCommand(CommandContext ctx,
            [RemainingText]
            [DescriptionAttribute("List of count followed by material ticker\nEx: `50 RAT 50 DW 10 OVE 5 PWO`   \nOptional last two arguments specifying capacity\n `100t 100m`")]
            string materialCountListStr)
        {
            var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(79, 138, 16));

            // Replace all non-alphanumerics from the materialCountListStr with a space
            materialCountListStr = Regex.Replace(materialCountListStr, @"[^a-zA-Z0-9\.]", " ");

            // Split on space
            var materialCountList = materialCountListStr.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            float weightAvailable = 500.0f;
            float volumeAvailable = 500.0f;
            GetWeightVolume(ref materialCountList, ref weightAvailable, ref volumeAvailable);
            if (weightAvailable < 0.0001)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Weight Invalid", $"Weight of {weightAvailable} is not valid.");
                await ctx.RespondAsync(response);
                return;
            }
            if (volumeAvailable < 0.001)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Volume Invalid", $"Volume of {volumeAvailable} is not valid.");
                await ctx.RespondAsync(response);
                return;
            }

            var counts = new List<float>();
            float weight = 0.0f;
            float volume = 0.0f;

            var stringCounts = materialCountList.Where((x, i) => i % 2 == 0).ToList();
            var tickers = materialCountList.Where((x, i) => i % 2 != 0).ToList();

            if (stringCounts.Count != tickers.Count)
            {
                // Bail, uneven arguments
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Arguments", "Arguments should be in count-ticker pairs");
                await ctx.RespondAsync(response);
                return;
            }

            // Verify counts are all float
            foreach (var stringCount in stringCounts)
            {
                float c;
                if (!float.TryParse(stringCount, out c))
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: FloatParse", $"Failed to parse '{stringCount}' as a float value.");
                    await ctx.RespondAsync(response);
                    return;
                }

                counts.Add(c);
            }

            var tickerWeights = new List<float>();
            var tickerVolumes = new List<float>();

            for (int i = 0; i < tickers.Count; ++i)
            {
                var ticker = tickers[i];
                var matPayload = await Materials.FindByTicker(ticker);
                if (matPayload == null)
                {
                    // Bad arg
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: Ticker", $"Failed to find ticker '{ticker}'.");
                    await ctx.RespondAsync(response);
                    return;
                }

                var tickerWeight = matPayload.Weight * counts[i];
                var tickerVolume = matPayload.Volume * counts[i];
                tickerWeights.Add(matPayload.Weight);
                tickerVolumes.Add(matPayload.Volume);

                weight += tickerWeight;
                volume += tickerVolume;
            }

            float weightRatio = weightAvailable / weight;
            float volumeRatio = volumeAvailable / volume;

            int ratioToUse = (int)((weightRatio < volumeRatio) ? weightRatio : volumeRatio);

            float totalWeight, totalVolume;
            List<int> materialCounts;
            GetRatioValues(counts, ratioToUse, tickerWeights, tickerVolumes, out totalWeight, out totalVolume, out materialCounts);

            if (totalWeight > weightAvailable || totalVolume > volumeAvailable)
            {
                ratioToUse -= 1;

                // The ceiling operation previous pushed us over.  The actual ratio is one less.
                GetRatioValues(counts, ratioToUse, tickerWeights, tickerVolumes, out totalWeight, out totalVolume, out materialCounts);
            }

            weightAvailable -= totalWeight;
            volumeAvailable -= totalVolume;

            if (ratioToUse == 0)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Does Not Fit", "Materials provided do not it in the space specified.");
                await ctx.RespondAsync(response);
            }
            else
            {
                response.AddField("Ratio", $"{ratioToUse}");

                StringBuilder sb = new StringBuilder();
                for (int tickerIdx = 0; tickerIdx < tickers.Count; ++tickerIdx)
                {
                    sb.AppendLine($"_{tickers[tickerIdx]}_: {materialCounts[tickerIdx]}");
                }
                response.AddField("Material Counts", sb.ToString());
                response.AddField("Weight Total", $"{totalWeight.ToString("N2")}t");
                response.AddField("Volume Total", $"{totalVolume.ToString("N2")}m³");
                response.AddField("Weight Remaining", $"{weightAvailable.ToString("N2")}t");
                response.AddField("Volume Remaining", $"{volumeAvailable.ToString("N2")}m³");
                await ctx.RespondAsync(response);
            }
        }

        private void GetRatioValues(List<float> counts, int ratioToTest, List<float> tickerWeights, List<float> tickerVolumes, out float weightUsed, out float volumeUsed, out List<int> materialCounts)
        {
            weightUsed = 0.0f;
            volumeUsed = 0.0f;
            materialCounts = new List<int>();

            var countsWithRatio = new List<float>(counts);
            countsWithRatio = countsWithRatio.Select(c => c * ratioToTest).ToList();

            for (int i = 0; i < countsWithRatio.Count; ++i)
            {
                int matCount = (int)Math.Ceiling(countsWithRatio[i]);
                materialCounts.Add(matCount);
                weightUsed += (matCount * tickerWeights[i]);
                volumeUsed += (matCount * tickerVolumes[i]);
            }
        }


        [Command("space")]
        [DescriptionAttribute("Get taken and remaining space by specified counts and the material tickers")]
        public async Task SpaceCommand(CommandContext ctx,
            [RemainingText]
            [DescriptionAttribute("List of count followed by material ticker\nEx: `200 H2O 50 CLI 50 RAT 50 DW`  \nOptional last two arguments specifying capacity:\n `100t 100m`")]
            string materialCountListStr)
        {
            var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(79, 138, 16));

            // Replace all non-alphanumerics from the materialCountListStr with a space
            materialCountListStr = Regex.Replace(materialCountListStr, @"[^a-zA-Z0-9\.]", " ");

            // Split on space
            var materialCountList = materialCountListStr.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            float weightAvailable = 500.0f;
            float volumeAvailable = 500.0f;
            GetWeightVolume(ref materialCountList, ref weightAvailable, ref volumeAvailable);
            if (weightAvailable < 0.0001)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Weight Invalid", $"Weight of {weightAvailable} is not valid.");
                await ctx.RespondAsync(response);
                return;
            }
            if (volumeAvailable < 0.001)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Volume Invalid", $"Volume of {volumeAvailable} is not valid.");
                await ctx.RespondAsync(response);
                return;
            }

            var counts = new List<int>();
            float weight = 0.0f;
            float volume = 0.0f;

            var stringCounts = materialCountList.Where((x, i) => i % 2 == 0).ToList();
            var tickers = materialCountList.Where((x, i) => i % 2 != 0).ToList();

            if (stringCounts.Count != tickers.Count)
            {
                // Bail, uneven arguments
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField("Error: Arguments", "Arguments should be in count-ticker pairs");
                await ctx.RespondAsync(response);
                return;
            }

            // Verify counts are all integers
            foreach (var stringCount in stringCounts)
            {
                int c;
                if (!int.TryParse(stringCount, out c))
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: IntParse", $"Failed to parse '{stringCount}' as an integer value.");
                    await ctx.RespondAsync(response);
                    return;
                }

                counts.Add(c);
            }

            // Verify tickers are all found
            for (int i = 0; i < tickers.Count; ++i)
            {
                var ticker = tickers[i];
                var matPayload = await Materials.FindByTicker(ticker);
                if (matPayload == null)
                {
                    // Bad arg
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: Ticker", $"Failed to find ticker '{ticker}'.");
                    await ctx.RespondAsync(response);
                    return;
                }

                weight += (matPayload.Weight * counts[i]);
                volume += (matPayload.Volume * counts[i]);
            }

            weightAvailable -= weight;
            volumeAvailable -= volume;

            response.AddField("Weight Total", $"{weight.ToString("N2")}t");
            response.AddField("Volume Total", $"{volume.ToString("N2")}m³");
            response.AddField("Weight Remaining", $"{weightAvailable.ToString("N2")}t");
            response.AddField("Volume Remaining", $"{volumeAvailable.ToString("N2")}m³");

            await ctx.RespondAsync(response);
        }

        [Command("math")]
        [DescriptionAttribute("Does math")]
        public async Task MathCommand(CommandContext ctx,
            [RemainingText]
            [DescriptionAttribute("The formula or equation you want to be solved.")]
            string mathStr)
        {
            Expression expr;
            double result;
            try
            {
                await ctx.TriggerTypingAsync();
                expr = new Expression(mathStr);
                result = expr.calculate();
                await ctx.RespondAsync(result.ToString("N2"));
            }
            catch
            {
                await ctx.RespondAsync("Failed to parse.");
            }
        }
    }
}
