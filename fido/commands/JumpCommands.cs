﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System;
using FIOWeb.JsonPayloads;
using Fido.Data;
using Serilog;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSharpPlus.Entities;
using fido.JsonPayloads;

namespace Fido.commands
{
    public class JumpCommands : BaseCommandModule
    {
        public JumpCommands()
        {

        }

        [Command("jumpcount")]
        [DescriptionAttribute("Get the ftl jump count between two planets or systems")]
        public async Task JumpCountCommand(CommandContext ctx,
            [DescriptionAttribute("Name or Natural ID of the source planet or system")]
            string source,
            [DescriptionAttribute("Name or Natural ID of the destination planet or system")]
            string destination)
        {
            _ = Task.Run(async () =>
            {
                Web.Request jumpCountRequest = new Web.Request(HttpMethod.Get, $"/systemstars/jumpcount/{source}/{destination}");
                int? jumpCount;
                jumpCount = await jumpCountRequest.GetResponseAsync<int?>();

                var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(79, 138, 16));
                if (jumpCount != null && jumpCountRequest.StatusCode == HttpStatusCode.OK)
                {
                    response.AddField("Source", source, true);
                    response.AddField("Destination", destination, true);
                    response.AddField("Jump Count", $"{(int)jumpCount}", true);
                    await ctx.RespondAsync(response);
                }
                else
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error", "Failed to resolve source or destination.");
                    await ctx.RespondAsync(response);
                }
            });

            await Task.FromResult(0);
        }

        [Command("jumproute")]
        [DescriptionAttribute("Get the ftl jump route between two planets or systems")]
        public async Task JumpRouteCommand(CommandContext ctx,
            [DescriptionAttribute("Name or Natural ID of the source planet or system")]
            string source,
            [DescriptionAttribute("Name or Natural ID of the destination planet or system")]
            string destination)
        {
            _ = Task.Run(async () =>
            {
                Web.Request jumpRouteRequest = new Web.Request(HttpMethod.Get, $"/systemstars/jumproute/{source}/{destination}");
                List<JumpRoutePayload> jumps = await jumpRouteRequest.GetResponseAsync<List<JumpRoutePayload>>();

                var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(79, 138, 16));
                if (jumps != null && jumpRouteRequest.StatusCode == HttpStatusCode.OK)
                {
                    response.AddField("Source", source, true);
                    response.AddField("Destination", destination, true);
                    response.AddField("Jump Count", $"{jumps.Count}");

                    double overallDistance = jumps.Select(j => j.Distance).Sum() / 12.0;
                    response.AddField("Distance (parsecs)", overallDistance.ToString("N2"));

                    var sb = new StringBuilder();
                    for (int i = 0; i < jumps.Count; ++i)
                    {
                        var jump = jumps[i];
                        var jumpDist = jump.Distance / 12.0;

                        string sourceStr = (jump.SourceSystemName != jump.SourceSystemNaturalId) ? $"{jump.SourceSystemName} ({jump.SourceSystemNaturalId})" : jump.SourceSystemNaturalId;
                        string destStr = (jump.DestinationSystemName != jump.DestinationNaturalId) ? $"{jump.DestinationSystemName} ({jump.DestinationNaturalId})" : jump.DestinationNaturalId;
                        sb.AppendLine($"{i + 1}: {sourceStr} -> {destStr} [{jumpDist.ToString("N2")}]");
                    }

                    if (sb.Length > 4096 )
                    {
                        response.WithDescription(sb.ToString().Substring(0, 4000) + "...\n(Path too large)");
                    }
                    else
                    {
                        response.WithDescription(sb.ToString());
                    }
                    
                    await ctx.RespondAsync(response);
                }
                else
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error", "Failed to resolve source or destination.");
                    await ctx.RespondAsync(response);
                }
            });

            await Task.FromResult(0);
        }
    }
}
