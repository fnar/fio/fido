﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using fido.Database;
using fido.Database.Models;
using fido.JsonPayloads;

using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using System.Net;
using Microsoft.Extensions.Caching.Memory;
using Fido.Data;
using fido.Extensions;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;

namespace Fido.commands
{
	internal static class FioCommandsHelper
	{
		public static void AddEntry(this SortedDictionary<string, List<string>> thisDict, string key, string value)
		{
			if (!thisDict.ContainsKey(key))
			{
				thisDict.Add(key, new List<string>());
			}

			thisDict[key].Add(value);
		}
	}

	public class FioCommands : BaseCommandModule
	{
		private static string FioAPIKey
		{
			get
			{
				if (_FioAPIKey == null)
				{
					if (Globals.config["fio"].HasKey("apikey"))
					{
						_FioAPIKey = Globals.config["fio"]["apikey"];
					}
				}

				return _FioAPIKey;
			}
		}
		private static string _FioAPIKey = null;

		public FioCommands()
		{

		}

		private void StripAndReportBadRequests(ref List<BurnPayload> payload, ref DiscordEmbedBuilder response)
		{
			var errors = payload.Where(p => !string.IsNullOrEmpty(p.Error));
			if (errors.Count() > 0)
			{
				var errorResponse = new StringBuilder();
				foreach (var error in errors)
				{
					errorResponse.AppendLine($"{error.UserName} - {error.Error}");
				}

				response.AddField("Errors:", errorResponse.ToString());
			}
			payload = payload.Where(p => string.IsNullOrEmpty(p.Error) && p.PlanetDisplayName != null).ToList();
		}

		private void ApplyLocationFilters(ref List<BurnPayload> payload, List<string> filters)
		{
			if (filters.Count > 0)
			{
				var filteredPayload = new List<BurnPayload>();
				foreach (var payloadEntry in payload)
				{
					foreach (var filter in filters)
					{
						if (payloadEntry.PlanetDisplayName.ToUpper().Contains(filter))
						{
							filteredPayload.Add(payloadEntry);
							break;
						}
					}
				}

				payload = filteredPayload;
			}
		}

		private void ApplyLocationFilters(ref List<PlayerModel> playerModels, List<string> filters)
		{
			if (filters.Count > 0)
			{
				// Filter PlayerModels
				var filteredPlayerModels = new List<PlayerModel>();
				foreach (var playerModel in playerModels)
				{
					var filteredPlayerModelLocations = new List<PlayerLocation>();
					var filteredStationaryPlayerShips = new List<PlayerShip>();
					foreach (var playerLocation in playerModel.Locations)
					{
						foreach (var filter in filters)
						{
							if (playerLocation.LocationDisplayName.ToUpper().Contains(filter))
							{
								filteredPlayerModelLocations.Add(playerLocation);
								filteredStationaryPlayerShips.AddRange(playerLocation.StationaryPlayerShips);
								break;
							}
						}
					}

					playerModel.Locations = filteredPlayerModelLocations;

					if (playerModel.Locations.Count > 0)
					{
						filteredPlayerModels.Add(playerModel);
					}
				}

				playerModels = filteredPlayerModels;
			}
		}

		private void ApplyLocationFilters(ref List<PlayerShip> playerShipsInFlight, List<string> filters)
		{
			if (filters.Count > 0)
			{
				// Filter ShipsInFlight
				var filteredPlayerShipsInFlight = new List<PlayerShip>();
				foreach (var playerShipInFlight in playerShipsInFlight)
				{
					foreach (var filter in filters)
					{
						if (playerShipInFlight.DisplayLocation != null && playerShipInFlight.DisplayLocation.ToUpper().Contains(filter))
						{
							filteredPlayerShipsInFlight.Add(playerShipInFlight);
							break;
						}
					}
				}

				playerShipsInFlight = filteredPlayerShipsInFlight;
			}
		}

		private void ApplyLocationFilters(ref List<GroupCXWarehouse> playerCXWarehouses, List<string> filters)
		{
			if (filters.Count > 0)
			{
				// Filter CXWarehouses
				var filteredCXWarehouses = new List<GroupCXWarehouse>();
				foreach (var playerCXWarehouse in playerCXWarehouses)
				{
					foreach (var filter in filters)
					{
						if (playerCXWarehouse.WarehouseLocationNaturalId.ToUpper().Contains(filter) || playerCXWarehouse.WarehouseLocationName.ToUpper().Contains(filter))
						{
							filteredCXWarehouses.Add(playerCXWarehouse);
							break;
						}
					}
				}

				playerCXWarehouses = filteredCXWarehouses;
			}
		}

		private void ApplyUserNameFilters(ref List<PlayerModel> playerModels, List<string> filters)
        {
			if (filters.Count > 0)
			{
				filters = filters.Select(f => f.ToUpper()).ToList();

				// Filter PlayerModels
				var filteredPlayerModels = new List<PlayerModel>();
				foreach (var playerModel in playerModels)
				{
					foreach (var filter in filters)
                    {
						if (playerModel.UserName.ToUpper().Contains(filter))
                        {
							filteredPlayerModels.Add(playerModel);
							break;
                        }
                    }
				}
				playerModels = filteredPlayerModels;
			}
		}

		private void ApplyBuildingFilters(ref List<PlayerModel> playerModels, string buildingFilter, double conditionThreshold)
		{
			for (int playerModelIdx = playerModels.Count - 1; playerModelIdx >= 0; playerModelIdx--)
			{
				var playerModel = playerModels[playerModelIdx];

				var filteredPlayerModelLocations = new List<PlayerLocation>();
				foreach (var playerLocation in playerModel.Locations)
				{
					if (buildingFilter != "*")
					{
						playerLocation.Buildings = playerLocation.Buildings.Where(b => b.BuildingTicker.ToUpper() == buildingFilter).ToList();
					}

					playerLocation.Buildings = playerLocation.Buildings.Where(b => b.Condition <= conditionThreshold && b.RepairMaterials.Count > 0).ToList();
					if (playerLocation.Buildings.Count > 0)
					{
						filteredPlayerModelLocations.Add(playerLocation);
					}
				}

				playerModel.Locations = filteredPlayerModelLocations;
				if (playerModel.Locations.Count == 0)
				{
					playerModels.RemoveAt(playerModelIdx);
				}
			}
		}

		private void ParseRemainingText(string remainingText, ref List<string> filters, ref ulong? groupId)
		{
			filters = new List<string>();
			groupId = null;

			if (!string.IsNullOrEmpty(remainingText))
			{
				filters = remainingText.ToUpper().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
				if (filters.Count > 0 && ulong.TryParse(filters[filters.Count - 1], out ulong CandidateGroupId))
				{
					groupId = CandidateGroupId;
					filters.RemoveAt(filters.Count - 1);
				}
			}
		}

		private async Task SetModelEntry(ulong ChannelId, ulong GroupId)
		{
			// Go ahead and set it since it's a DM
			using (var transaction = new FIDODbTransaction())
			{
				var entry = await transaction.DB.DefaultGroupIds.FirstOrDefaultAsync(dgi => dgi.DiscordChannelId == ChannelId);
				if (entry == null)
				{
					entry = new DefaultGroupIdModel();
					entry.DiscordChannelId = ChannelId;
					entry.GroupId = GroupId;
					transaction.DB.DefaultGroupIds.Add(entry);
				}
				else
				{
					entry.GroupId = GroupId;
				}

				await transaction.DB.SaveChangesAsync();
			}
		}

		private async Task<ulong?> GetDefaultGroupId(ulong ChannelId)
		{
			using (var DB = new FIDODbContext())
			{
				var entry = await DB.DefaultGroupIds.FirstOrDefaultAsync(dgi => dgi.DiscordChannelId == ChannelId);
				return entry?.GroupId;
			}
		}

		private string GetLastUpdatedString(DateTime LastUpdated)
		{
			StringBuilder sb = new StringBuilder();

			TimeSpan ts = DateTime.UtcNow - LastUpdated;
			if (ts.TotalDays >= 1.0)
			{
				int days = (int)ts.TotalDays;
				sb.Append(days.ToString());
				sb.Append("d");
			}
			if (ts.Hours > 0)
			{
				sb.Append(ts.Hours.ToString());
				sb.Append("h");
			}

			if (sb.Length > 0)
			{
				return $" [{sb} ago]";
			}

			return "";
		}

		[Command("setdefaultgroupid")]
		public async Task SetDefaultGroupId(CommandContext ctx,
			[DescriptionAttribute("GroupId")]
			ulong groupId)
		{
			bool bAssigned = false;
			ulong ChannelId = ctx.Channel.Id;

			bool bIsDM = (ctx.Guild == null);
			if (bIsDM)
			{
				bAssigned = true;
				await SetModelEntry(ChannelId, groupId);
			}
			else
			{
				// Only allow setting this channel if the user has administrative privileges
				if (ctx.Member.IsOwner || ctx.Member.Permissions.HasFlag(DSharpPlus.Permissions.Administrator))
				{
					bAssigned = true;
					await SetModelEntry(ChannelId, groupId);
				}
			}

			if (bAssigned)
			{
				await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, ":white_check_mark:"));
			}
			if (!bAssigned)
			{
				await ctx.RespondAsync("You must be server owner or administrator to issue this command.");
			}
		}

		public static TimeSpan DataExpireTime = TimeSpan.FromMinutes(10);
		public static MemoryCache cache = new MemoryCache(new MemoryCacheOptions());

		public void ClearMemoryCache(CommandContext ctx, ulong groupId)
		{
            var ProxyRequesterDiscordId = ctx.User.GetFullyQualifiedUsername();
            cache.Remove($"GROUPHUB-{ProxyRequesterDiscordId}-{groupId}");
			cache.Remove($"BURN-{ProxyRequesterDiscordId}-{groupId}");
		}

		public async Task<Tuple<string, GroupHubPayload>> GetGroupHubPayload(CommandContext ctx, ulong groupId)
		{
			var ProxyRequesterDiscordId = ctx.User.GetFullyQualifiedUsername();
            var CacheKey = $"GROUPHUB-{ProxyRequesterDiscordId}-{groupId}";

			string ErrorMessage = null;

			var GroupHubPayload = cache.Get<GroupHubPayload>(CacheKey);
			if (GroupHubPayload == null)
			{
				var groupRequest = new Web.Request(HttpMethod.Get, $"/auth/group/{groupId}", FioAPIKey);
				var group = await groupRequest.GetResponseAsync<GroupPayload>();
				if (group != null)
				{
					var usersToRetrieve = group.GroupUsers.Select(gu => gu.GroupUserName).ToList();
					var groupHubRequest = new Web.Request(HttpMethod.Post, "/fioweb/grouphub", FioAPIKey, JsonConvert.SerializeObject(usersToRetrieve));

					groupHubRequest.AddHeader("ProxyRequesterDiscordId", ProxyRequesterDiscordId);
					GroupHubPayload = await groupHubRequest.GetResponseAsync<GroupHubPayload>();
					if (GroupHubPayload != null)
					{
						cache.Set(CacheKey, GroupHubPayload, DataExpireTime);
					}
					else
					{
						if (groupHubRequest.StatusCode == HttpStatusCode.Unauthorized)
						{
							ErrorMessage = "You must associate your DiscordId with your FIO account.";
						}
						else
						{
							ErrorMessage = "Failed to retrieve group data.";
						}
					}
				}
				else
				{

					ErrorMessage = "Group Not Found.";
				}
			}

			GroupHubPayload = GroupHubPayload != null ? (GroupHubPayload)GroupHubPayload.Clone() : null;
			return new Tuple<string, GroupHubPayload>(ErrorMessage, GroupHubPayload);
		}

		public async Task<Tuple<string, List<BurnPayload>>> GetBurnPayload(CommandContext ctx, ulong groupId)
		{
            var ProxyRequesterDiscordId = ctx.User.GetFullyQualifiedUsername();
            var CacheKey = $"BURN-{ProxyRequesterDiscordId}-{groupId}";

			string ErrorMessage = null;

			var BurnPayload = cache.Get<List<BurnPayload>>(CacheKey);
			if (BurnPayload == null)
			{
				var burnRequest = new Web.Request(HttpMethod.Get, $"/fioweb/burn/group/{groupId}", FioAPIKey);
				burnRequest.AddHeader("ProxyRequesterDiscordId", ProxyRequesterDiscordId);
				BurnPayload = await burnRequest.GetResponseAsync<List<BurnPayload>>();
				BurnPayload = BurnPayload.Where(bp => String.IsNullOrEmpty(bp.Error)).ToList();
				if (BurnPayload != null)
				{
					cache.Set(CacheKey, BurnPayload, DataExpireTime);
				}
				else
				{
					if (burnRequest.StatusCode == HttpStatusCode.Unauthorized)
					{
						ErrorMessage = "You must associate your DiscordId with your FIO account.";
					}
					else
					{
						ErrorMessage = "Failed to retrieve group data.";
					}
				}
			}

			var clonedBurnPayload = new List<BurnPayload>();
			BurnPayload.ForEach((item) => { clonedBurnPayload.Add((BurnPayload)item.Clone()); });
			return new Tuple<string, List<BurnPayload>>(ErrorMessage, clonedBurnPayload);
		}

		[Command("clearcache")]
		[DescriptionAttribute("Clear the cache for a group id (falling back to default groupid). Example: `clearcache 123456")]
		public async Task ClearCacheCommand(CommandContext ctx,
			[RemainingText]
			[DescriptionAttribute("Group Id")]
			string remainingText)
		{
			_ = Task.Run(async () =>
			{
				ulong? groupId = null;
				if (!string.IsNullOrWhiteSpace(remainingText))
				{
					if (ulong.TryParse(remainingText, out ulong parsedGroupId))
					{
						groupId = parsedGroupId;
					}
				}

				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				if (groupId != null)
				{
					ClearMemoryCache(ctx, (ulong)groupId);
					await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, ":white_check_mark:"));
				}
				else
				{
					await ctx.RespondAsync("Invalid or no groupid specified.");
				}
			});

			await Task.FromResult(0);
		}

		[Command("findmaterial"), Aliases("findmaterials", "findmat", "findmats")]
		[DescriptionAttribute("Find the material given a a material ticker, optional planet filters, and a group id that is last. Example: `findmat RAT Proxion 123456`")]
		public async Task FindMatCommand(CommandContext ctx,
			[DescriptionAttribute("Material ticker")]
			string material,
			[RemainingText]
			[DescriptionAttribute("Planet specifiers and the group id")]
			string remainingText)
		{
			material = material.ToUpper();

            _ = Task.Run(async () =>
            {
                await ctx.TriggerTypingAsync();

                List<string> filters = null;
                ulong? groupId = null;
                ParseRemainingText(remainingText, ref filters, ref groupId);
                groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

                var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
                if (groupId == null)
                {
                    response.AddField("Error", "GroupId not specified or set.");
                    await ctx.RespondAsync(response);
                    return;
                }

                var res = await GetGroupHubPayload(ctx, (ulong)groupId);
                var ErrorMessage = res.Item1;
                if (ErrorMessage != null)
                {
                    response.AddField("Error", ErrorMessage);
                    await ctx.RespondAsync(response);
                    return;
                }

                var groupHubPayload = res.Item2;
                response.WithColor(new DiscordColor(79, 138, 16));

                if (groupHubPayload.Failures.Count > 0)
                {
					response.AddField("Errors", string.Join("\n", groupHubPayload.Failures));
                }

                var playerModels = groupHubPayload.PlayerModels.OrderBy(pm => pm.UserName).ToList();
                var playerShipsInFlight = groupHubPayload.PlayerShipsInFlight;
                var playerCXWarehouses = groupHubPayload.CXWarehouses;
                ApplyLocationFilters(ref playerModels, filters);
                ApplyLocationFilters(ref playerShipsInFlight, filters);
                ApplyLocationFilters(ref playerCXWarehouses, filters);

                var locationToResult = new SortedDictionary<string, List<string>>();

                int totalUnits = 0;

                // First, search bases, warehouses, and stationary planet ships
                foreach (var playerModel in playerModels)
                {
                    var UserName = playerModel.UserName;
                    foreach (var playerLocation in playerModel.Locations)
                    {
                        var LocationName = playerLocation.LocationDisplayName;
                        if (String.IsNullOrEmpty(LocationName))
                        {
                            continue;
                        }

                        if (playerLocation.BaseStorage != null)
                        {
                            var baseMatch = playerLocation.BaseStorage.Items.FirstOrDefault(i => i.MaterialTicker == material);
                            if (baseMatch != null)
                            {
                                totalUnits += baseMatch.Units;
                                locationToResult.AddEntry(LocationName, $"**{UserName}**: {baseMatch.Units}{GetLastUpdatedString(playerLocation.BaseStorage.LastUpdated)}");
                            }
                        }

                        if (playerLocation.WarehouseStorage != null)
                        {
                            var warehouseMatch = playerLocation.WarehouseStorage.Items.FirstOrDefault(i => i.MaterialTicker == material);
                            if (warehouseMatch != null)
                            {
                                totalUnits += warehouseMatch.Units;
                                locationToResult.AddEntry(LocationName, $"**{UserName}**-WAR: {warehouseMatch.Units}");
                            }
                        }

                        foreach (var ship in playerLocation.StationaryPlayerShips)
                        {
                            var cargoItem = ship.Cargo.Items.FirstOrDefault(i => i.MaterialTicker == material);
                            if (cargoItem != null)
                            {
                                totalUnits += cargoItem.Units;
                                locationToResult.AddEntry($"{LocationName}", $"**{UserName}**-Ship-{ship.ShipDisplayName}: {cargoItem.Units}{GetLastUpdatedString(ship.LastUpdated)}");
                            }
                        }
                    }
                }

                // Next, search in-flight ships
                foreach (var playerShipInFlight in playerShipsInFlight)
                {
                    if (playerShipInFlight.DisplayLocation != null)
                    {
                        var UserName = playerShipInFlight.PlayerName;
                        var cargoItem = playerShipInFlight.Cargo.Items.FirstOrDefault(i => i.MaterialTicker == material);
                        if (cargoItem != null)
                        {
                            totalUnits += cargoItem.Units;
                            locationToResult.AddEntry($"{playerShipInFlight.DisplayLocation}", $"**{UserName}**-Ship-{playerShipInFlight.ShipDisplayName}: {cargoItem.Units}{GetLastUpdatedString(playerShipInFlight.LastUpdated)}");
                        }
                    }

                }

                // Finally, search CXWarehouses
                foreach (var playerCXWarehouse in playerCXWarehouses)
                {
                    var LocationNaturalId = playerCXWarehouse.WarehouseLocationNaturalId;

                    foreach (var playerCX in playerCXWarehouse.PlayerCXWarehouses)
                    {
                        var UserName = playerCX.PlayerName;
                        var item = playerCX.Items.FirstOrDefault(i => i.MaterialTicker == material);
                        if (item != null)
                        {
                            totalUnits += item.Units;
                            locationToResult.AddEntry(LocationNaturalId, $"**{UserName}**-WAR: {item.Units}");
                        }
                    }
                }

				var pages = new List<Page>();
                if (locationToResult.Count > 0)
                {
                    if (totalUnits > 0)
                    {
                        response.WithFooter($"Total: {totalUnits}");
                    }

                    foreach (var kvp in locationToResult)
                    {
                        response.FIOAddField(kvp.Key, string.Join("\n", kvp.Value), ref pages);
                    }

					if (pages.Count > 0)
					{
						// Paginated
						pages.Add(new Page(embed: response));
                        await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                    }
					else
					{
                        await ctx.RespondAsync(response);
                    }
                }
                else
                {
                    response.AddField("No Results", "No results found.");
                    await ctx.RespondAsync(response);
                }
            });

            await Task.FromResult(0);
		}

		[Command("findrepair"), Aliases("findrepairs", "findrep")]
		[DescriptionAttribute("Finds repairs for a given building ticker (`*` signifies all buildings), optional planet specifiers, and groupid.")]
		public async Task FindRepairCommand(CommandContext ctx,
			[DescriptionAttribute("Building ticker or * for all buildings")]
			string buildingTicker,
			[DescriptionAttribute("Condition threshold")]
			double conditionThreshold,
			[RemainingText]
			[DescriptionAttribute("Optional planet specifiers, and groupid.")]
			string remainingText)
		{
			buildingTicker = buildingTicker.ToUpper();

			if (conditionThreshold > 1.0)
			{
				// treat it as a percentage
				conditionThreshold /= 100;
			}

			_ = Task.Run(async () =>
			{
				await ctx.TriggerTypingAsync();

                List<string> filters = null;
				ulong? groupId = null;
				ParseRemainingText(remainingText, ref filters, ref groupId);
				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
				if (groupId == null)
				{
					response.AddField("Error", "GroupId not specified or set.");
					await ctx.RespondAsync(response);
					return;
				}

				var res = await GetGroupHubPayload(ctx, (ulong)groupId);
				var ErrorMessage = res.Item1;
				if (ErrorMessage != null)
				{
					response.AddField("Error", ErrorMessage);
					await ctx.RespondAsync(response);
					return;
				}

				var groupHubPayload = res.Item2;
				response.WithColor(new DiscordColor(79, 138, 16));

				if (groupHubPayload.Failures.Count > 0)
				{
					response.AddField("Errors:", string.Join("\n", groupHubPayload.Failures));
				}

				var playerModels = groupHubPayload.PlayerModels.OrderBy(pm => pm.UserName).ToList();
				ApplyLocationFilters(ref playerModels, filters);
				ApplyBuildingFilters(ref playerModels, buildingTicker, conditionThreshold);

				var totalRepairs = new SortedDictionary<string, int>();

				var locationToResult = new SortedDictionary<string, List<string>>();
				foreach (var playerModel in playerModels)
				{
					var UserName = playerModel.UserName;
					foreach (var playerLocation in playerModel.Locations)
					{
						string LocationName = playerLocation.LocationDisplayName;

						var repairMatToCount = new SortedDictionary<string, int>();
						foreach (var rep in playerLocation.Buildings.SelectMany(b => b.RepairMaterials))
						{
							if (!repairMatToCount.ContainsKey(rep.MaterialTicker))
							{
								repairMatToCount.Add(rep.MaterialTicker, rep.Units);
							}
							else
							{
								repairMatToCount[rep.MaterialTicker] += rep.Units;
							}

							if (!totalRepairs.ContainsKey(rep.MaterialTicker))
							{
								totalRepairs.Add(rep.MaterialTicker, rep.Units);
							}
							else
							{
								totalRepairs[rep.MaterialTicker] += rep.Units;
							}
						}

						var repairSb = new StringBuilder();
						foreach (var kvp in repairMatToCount)
						{
							repairSb.Append($"{kvp.Value} {kvp.Key}, ");
						}
						repairSb.Remove(repairSb.Length - 2, 2);

						locationToResult.AddEntry(LocationName, $"**{UserName}**: {repairSb}");
					}
				}

				var pages = new List<Page>();
				if (locationToResult.Count > 0)
				{
                    var totalRepairsSb = new StringBuilder();
                    foreach (var kvp in totalRepairs)
                    {
                        totalRepairsSb.AppendLine($"{kvp.Value} {kvp.Key}");
                    }
                    response.WithFooter($"Totals:\n{totalRepairsSb}");

                    foreach (var kvp in locationToResult)
					{
						response.FIOAddField(kvp.Key, string.Join("\n", kvp.Value), ref pages);
					}
				}
				else
				{
					response.FIOAddField("None Found", "No repairs needed at location/threshold provided.", ref pages);
				}

                if (pages.Count > 0)
                {
                    // Paginated
                    pages.Add(new Page(embed: response));
                    await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                }
                else
                {
                    await ctx.RespondAsync(response);
                }
			});

			await Task.FromResult(0);
		}

		[Command("findbui"), Aliases("findbuis", "findbuilding", "findbuildings")]
		[DescriptionAttribute("Finds the building for a given ticker, optional planet specifiers, and groupid.")]
		public async Task FindBuiCommand(CommandContext ctx,
			[DescriptionAttribute("Building ticker")]
			string buildingTicker,
			[RemainingText]
			[DescriptionAttribute("Planet specifiers and the group id")]
			string remainingText)
		{
			buildingTicker = buildingTicker.ToUpper();

			_ = Task.Run(async () =>
			{
				await ctx.TriggerTypingAsync();

				List<string> filters = null;
				ulong? groupId = null;
				ParseRemainingText(remainingText, ref filters, ref groupId);
				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
				if (groupId == null)
				{
					response.AddField("Error", "GroupId not specified or set.");
					await ctx.RespondAsync(response);
					return;
				}

				var buildingPayload = await Buildings.FindByTicker(buildingTicker);
				if (buildingPayload == null)
				{
					response.AddField("Error", "Building Ticker specified is invalid.");
					await ctx.RespondAsync(response);
					return;
				}

				var res = await GetGroupHubPayload(ctx, (ulong)groupId);
				var ErrorMessage = res.Item1;
				if (ErrorMessage != null)
				{
					response.AddField("Error", ErrorMessage);
					await ctx.RespondAsync(response);
					return;
				}

				var groupHubPayload = res.Item2;
				response.WithColor(new DiscordColor(79, 138, 16));

				if (groupHubPayload.Failures.Count > 0)
				{
					response.AddField("Errors:", string.Join("\n", groupHubPayload.Failures));
				}

				var playerModels = groupHubPayload.PlayerModels.OrderBy(pm => pm.UserName).ToList();

				ApplyLocationFilters(ref playerModels, filters);

				var pages = new List<Page>();
				var locationToResult = new SortedDictionary<string, List<string>>();
				foreach (var playerModel in playerModels)
				{
					var UserName = playerModel.UserName;
					foreach (var playerLocation in playerModel.Locations)
					{
						var LocationName = playerLocation.LocationDisplayName;

						var ProdLine = playerLocation.ProductionLines.FirstOrDefault(pl => pl.BuildingTicker.ToUpper() == buildingTicker);
						if (ProdLine != null)
						{
							locationToResult.AddEntry(LocationName, $"**{UserName}**: {ProdLine.Capacity}");
						}
					}
				}

				if (locationToResult.Count > 0)
				{
					foreach (var kvp in locationToResult)
					{
						response.FIOAddField(kvp.Key, string.Join("\n", kvp.Value), ref pages);
					}
				}
				else
				{
					response.FIOAddField("Error", "No results.", ref pages);
				}

                if (pages.Count > 0)
                {
                    // Paginated
                    pages.Add(new Page(embed: response));
                    await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                }
                else
                {
                    await ctx.RespondAsync(response);
                }
			});

			await Task.FromResult(0);
		}

		[Command("findproducer"), Aliases("findproducers", "findprod", "findprods")]
		[DescriptionAttribute("Find who produces a material given a provided groupid, material ticker, and optional planet specifier")]
		public async Task FindProducerCommand(CommandContext ctx,
			[DescriptionAttribute("Material ticker")]
			string material,
			[RemainingText]
			[DescriptionAttribute("Planet specifiers and the group id")]
			string remainingText)
		{
			material = material.ToUpper();

			_ = Task.Run(async () =>
			{
				await ctx.TriggerTypingAsync();

				List<string> filters = null;
				ulong? groupId = null;
				ParseRemainingText(remainingText, ref filters, ref groupId);
				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
				if (groupId == null)
				{
					response.AddField("Error", "GroupId not specified or set.");
					await ctx.RespondAsync(response);
					return;
				}

				var res = await GetBurnPayload(ctx, (ulong)groupId);
				var ErrorMessage = res.Item1;
				if (ErrorMessage != null)

				{
					response.AddField("Error", ErrorMessage);
					await ctx.RespondAsync(response);
					return;
				}

				var payload = res.Item2;
				StripAndReportBadRequests(ref payload, ref response);

				ApplyLocationFilters(ref payload, filters);

				var matches = payload.Select(p => new
				{
					UserName = p.UserName,
					PlanetDisplayName = p.PlanetDisplayName,
					Productions = p.OrderProduction.Where(op => op.MaterialTicker == material).ToList(),
					NumUnits = p.Inventory.Where(i => i.MaterialTicker == material).Sum(i => i.MaterialAmount),
					LastUpdated = p.LastUpdate
				}).Where(m => m.Productions.Count > 0).ToList();

				var pages = new List<Page>();
				var planetToResult = new SortedDictionary<string, List<string>>();
				foreach (var match in matches)
				{
					var overallProduction = match.Productions.Sum(p => p.DailyAmount);
					planetToResult.AddEntry(match.PlanetDisplayName, $"**{match.UserName}**: {overallProduction:N2}/d ({match.NumUnits} avail){GetLastUpdatedString(match.LastUpdated)}");
				}

				if (planetToResult.Count > 0)
				{
					response.WithColor(new DiscordColor(79, 138, 16));
					foreach (var kvp in planetToResult)
					{
						response.FIOAddField(kvp.Key, string.Join("\n", kvp.Value), ref pages);
					}
				}
				else
				{
					response.AddField("Error", "No results");
				}

                if (pages.Count > 0)
                {
                    // Paginated
                    pages.Add(new Page(embed: response));
                    await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                }
                else
                {
                    await ctx.RespondAsync(response);
                }
			});

			await Task.FromResult(0);
		}

		[Command("findconsumer"), Aliases("findconsumers", "findconsume", "findconsumes")]
		[DescriptionAttribute("Find who consumes a material given a provided groupid, material ticker, and optional planet specifier.\nNote this will only return inputs to production lines.")]
		public async Task FindConsumerCommand(CommandContext ctx,
			[DescriptionAttribute("Material ticker")]
			string material,
			[RemainingText]
			[DescriptionAttribute("Planet specifiers and the group id")]
			string remainingText)
		{
			material = material.ToUpper();

			_ = Task.Run(async () =>
			{
				await ctx.TriggerTypingAsync();

				List<string> filters = null;
				ulong? groupId = null;
				ParseRemainingText(remainingText, ref filters, ref groupId);
				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
				if (groupId == null)
				{
					response.AddField("Error", "GroupId not specified or set.");
					await ctx.RespondAsync(response);
					return;
				}

				var res = await GetBurnPayload(ctx, (ulong)groupId);
				var ErrorMessage = res.Item1;
				if (ErrorMessage != null)
				{
					response.AddField("Error", ErrorMessage);
					await ctx.RespondAsync(response);
					return;
				}

				var payload = res.Item2;
				StripAndReportBadRequests(ref payload, ref response);

				ApplyLocationFilters(ref payload, filters);

				var matches = payload.Select(p => new
				{
					UserName = p.UserName,
					PlanetDisplayName = p.PlanetDisplayName,
					Consumptions = p.OrderConsumption.Where(op => op.MaterialTicker == material).ToList(),
					NumUnits = p.Inventory.Where(i => i.MaterialTicker == material).Sum(i => i.MaterialAmount),
					LastUpdated = p.LastUpdate
				}).Where(m => m.Consumptions.Count > 0).ToList();

				var planetToResult = new SortedDictionary<string, List<string>>();
				foreach (var match in matches)
				{
					var overallConsumption = match.Consumptions.Sum(p => p.DailyAmount);
					planetToResult.AddEntry(match.PlanetDisplayName, $"**{match.UserName}**: {overallConsumption:N2}/d ({match.NumUnits} avail){GetLastUpdatedString(match.LastUpdated)}");
				}

				var pages = new List<Page>();
				if (planetToResult.Count > 0)
				{
					response.WithColor(new DiscordColor(79, 138, 16));
					foreach (var kvp in planetToResult)
					{
						response.FIOAddField(kvp.Key, string.Join("\n", kvp.Value), ref pages);
					}
				}
				else
				{
					response.AddField("No results", "No results were found.");
				}

                if (pages.Count > 0)
                {
                    // Paginated
                    pages.Add(new Page(embed: response));
                    await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                }
                else
                {
                    await ctx.RespondAsync(response);
                }
            });

			await Task.FromResult(0);
		}

		[Command("findburn")]
		[DescriptionAttribute("Finds the burn for a material given a provided groupid, material ticker, and optional planet specifiers.\nNote this includes workforce burn.")]
		public async Task FindBurnCommand(CommandContext ctx,
			[DescriptionAttribute("Material ticker")]
			string material,
			[RemainingText]
			[DescriptionAttribute("Planet specifiers and the group id")]
			string remainingText)
		{
			material = material.ToUpper();

			_ = Task.Run(async () =>
			{
				await ctx.TriggerTypingAsync();

				List<string> filters = null;
				ulong? groupId = null;
				ParseRemainingText(remainingText, ref filters, ref groupId);
				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
				if (groupId == null)
				{
					response.AddField("Error", "GroupId not specified or set.");
					await ctx.RespondAsync(response);
					return;
				}

				var res = await GetBurnPayload(ctx, (ulong)groupId);
				var ErrorMessage = res.Item1;
				if (ErrorMessage != null)
				{
					response.AddField("Error", ErrorMessage);
					await ctx.RespondAsync(response);
					return;
				}

				var payload = res.Item2;
				StripAndReportBadRequests(ref payload, ref response);

				ApplyLocationFilters(ref payload, filters);

				var matches = payload.Select(p => new
				{
					UserName = p.UserName,
					PlanetDisplayName = p.PlanetDisplayName,
					OrderProduction = p.OrderProduction.Where(op => op.MaterialTicker == material).ToList(),
					OrderConsumption = p.OrderConsumption.Where(oc => oc.MaterialTicker == material).ToList(),
					WorkforceConsumption = p.WorkforceConsumption.Where(wc => wc.MaterialTicker == material).ToList(),
					NumUnits = p.Inventory.Where(i => i.MaterialTicker == material).Sum(i => i.MaterialAmount),
					LastUpdated = p.LastUpdate
				}).Where(m => m.OrderConsumption.Count > 0 || m.WorkforceConsumption.Count > 0 || m.OrderProduction.Count > 0).ToList();

				var planetToResult = new SortedDictionary<string, List<string>>();

				double totalConsumption = 0.0;
				double totalProduction = 0.0;
				int totalUnits = 0;
				foreach (var match in matches)
				{
					var overallOrderConsumption = match.OrderConsumption.Sum(oc => oc.DailyAmount);
					var overallWorkerConsumption = match.WorkforceConsumption.Sum(wc => wc.DailyAmount);
					var overallConsumption = overallOrderConsumption + overallWorkerConsumption;
					totalConsumption += overallConsumption;

					var overallOrderProduction = match.OrderProduction.Sum(op => op.DailyAmount);
					totalProduction += overallOrderProduction;

					totalUnits += match.NumUnits;

					var delta = overallOrderProduction - overallConsumption;

					string daysRemainingStr;
					if (delta < 0.0)
					{
						var daysRemaining = (double)match.NumUnits / (-delta);
						daysRemainingStr = $"{daysRemaining:N1}d remaining";
					}
					else
					{
						daysRemainingStr = "∞d remaining";
					}

					string deltaStr = delta >= 0.0 ? $"+{delta:N2}" : $"{delta:N2}";
					planetToResult.AddEntry(match.PlanetDisplayName, $"**{match.UserName}**: {deltaStr}/d ({daysRemainingStr}){GetLastUpdatedString(match.LastUpdated)}");
				}

				var pages = new List<Page>();
				if (planetToResult.Count > 0)
				{
                    if (totalConsumption > 0.0 || totalProduction > 0.0)
                    {
                        List<string> overallItems = new List<string>();

                        if (totalConsumption > 0.0)
                        {
                            overallItems.Add($"Daily Consumption: {totalConsumption:N2}/d");
                        }

                        if (totalProduction > 0.0)
                        {
                            overallItems.Add($"Daily Production: {totalProduction:N2}/d");
                        }

                        if (totalUnits > 0)
                        {
                            if (totalConsumption > totalProduction)
                            {
                                var totalDaysRemaining = (double)totalUnits / (totalConsumption - totalProduction);
                                overallItems.Add($"Days Remaining: {totalDaysRemaining:N1}");
                            }
                            else
                            {
                                overallItems.Add("Days Remaining: ∞");
                            }
                        }

                        var overallStr = "Overall:\n" + string.Join("\n", overallItems);
                        response.WithFooter(overallStr);
                    }

                    response.WithColor(new DiscordColor(79, 138, 16));
					foreach (var kvp in planetToResult)
					{
						response.FIOAddField(kvp.Key, string.Join("\n", kvp.Value), ref pages);
					}
				}
				else
				{
					response.AddField("No results", "No results were found.");
				}

                if (pages.Count > 0)
                {
                    // Paginated
                    pages.Add(new Page(embed: response));
                    await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                }
                else
                {
                    await ctx.RespondAsync(response);
                }
            });

			await Task.FromResult(0);
		}

		[Command("findpotentialconsumer"), Aliases("findpotentialcons", "findpotentialconsume", "findpotentialconsumes")]
		[DescriptionAttribute("Find who can consume a material given a provided groupid and optional planet specifier")]
		public async Task FindPotentialConsumerCommand(CommandContext ctx,
			[DescriptionAttribute("Material ticker")]
			string material,
			[RemainingText]
			[DescriptionAttribute("Planet specifiers and the group id")]
			string remainingText)
		{
			material = material.ToUpper();

			_ = Task.Run(async () =>
			{
				await ctx.TriggerTypingAsync();

				var buildingsToLookFor = await Buildings.FindByInput(material);

				List<string> filters = null;
				ulong? groupId = null;
				ParseRemainingText(remainingText, ref filters, ref groupId);
				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
				if (groupId == null)
				{
					response.AddField("Error", "GroupId not specified or set.");
					await ctx.RespondAsync(response);
					return;
				}

				var res = await GetGroupHubPayload(ctx, (ulong)groupId);
				var ErrorMessage = res.Item1;
				if (ErrorMessage != null)
				{
					response.AddField("Error", ErrorMessage);
					await ctx.RespondAsync(response);
					return;
				}

				var groupHubPayload = res.Item2;
				response.WithColor(new DiscordColor(79, 138, 16));

				if (groupHubPayload.Failures.Count > 0)
				{
					response.AddField("Errors:", string.Join("\n", groupHubPayload.Failures));
				}

				var playerModels = groupHubPayload.PlayerModels.OrderBy(pm => pm.UserName).ToList();

				ApplyLocationFilters(ref playerModels, filters);

				var locationToResult = new SortedDictionary<string, List<string>>();
				foreach (var playerModel in playerModels)
				{
					var UserName = playerModel.UserName;
					foreach (var playerLocation in playerModel.Locations)
					{
						var LocationName = playerLocation.LocationDisplayName;

						var playerBuildings = playerLocation.ProductionLines.Select(pl => pl.BuildingTicker.ToUpper()).ToList();
						var checkAgainstBuildings = buildingsToLookFor.Select(b => b.Ticker.ToUpper()).ToList();
						var intersects = playerBuildings.Intersect(checkAgainstBuildings);

						foreach (var intersect in intersects)
						{
							locationToResult.AddEntry(LocationName, $"**{UserName}** - {intersect}");
						}
					}
				}

				var pages = new List<Page>();
				if (locationToResult.Count > 0)
				{
					foreach (var kvp in locationToResult)
					{
						response.FIOAddField(kvp.Key, string.Join("\n", kvp.Value), ref pages);
					}
				}
				else
				{
					response.AddField("Error", "No results.");
				}

                if (pages.Count > 0)
                {
                    // Paginated
                    pages.Add(new Page(embed: response));
                    await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                }
                else
                {
                    await ctx.RespondAsync(response);
                }
            });

			await Task.FromResult(0);
		}

		[Command("findpotentialproducer"), Aliases("findpotentialprod", "findpotentialprods")]
		[DescriptionAttribute("Find who can produce a material given a provided groupid and optional planet specifier")]
		public async Task FindPotentialProducerCommand(CommandContext ctx,
			[DescriptionAttribute("Material ticker")]
			string material,
			[RemainingText]
			[DescriptionAttribute("Planet specifiers and the group id")]
			string remainingText)
		{
			material = material.ToUpper();

			_ = Task.Run(async () =>
			{
				await ctx.TriggerTypingAsync();

				var buildingsToLookFor = await Buildings.FindByOutput(material);

				List<string> filters = null;
				ulong? groupId = null;
				ParseRemainingText(remainingText, ref filters, ref groupId);
				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
				if (groupId == null)
				{
					response.AddField("Error", "GroupId not specified or set.");
					await ctx.RespondAsync(response);
					return;
				}

				var res = await GetGroupHubPayload(ctx, (ulong)groupId);
				var ErrorMessage = res.Item1;
				if (ErrorMessage != null)
				{
					response.AddField("Error", ErrorMessage);
					await ctx.RespondAsync(response);
					return;
				}

				var groupHubPayload = res.Item2;
				response.WithColor(new DiscordColor(79, 138, 16));

				if (groupHubPayload.Failures.Count > 0)
				{
					response.AddField("Errors:", string.Join("\n", groupHubPayload.Failures));
				}

				var playerModels = groupHubPayload.PlayerModels.OrderBy(pm => pm.UserName).ToList();

				ApplyLocationFilters(ref playerModels, filters);

				var locationToResult = new SortedDictionary<string, List<string>>();
				foreach (var playerModel in playerModels)
				{
					var UserName = playerModel.UserName;
					foreach (var playerLocation in playerModel.Locations)
					{
						var LocationName = playerLocation.LocationDisplayName;

						var playerBuildings = playerLocation.ProductionLines.Select(pl => pl.BuildingTicker.ToUpper()).ToList();
						var checkAgainstBuildings = buildingsToLookFor.Select(b => b.Ticker.ToUpper()).ToList();
						var intersects = playerBuildings.Intersect(checkAgainstBuildings);

						foreach (var intersect in intersects)
						{
							locationToResult.AddEntry(LocationName, $"**{UserName}** - {intersect}");
						}
					}
				}

				var pages = new List<Page>();
				if (locationToResult.Count > 0)
				{
					foreach (var kvp in locationToResult)
					{
						response.FIOAddField(kvp.Key, string.Join("\n", kvp.Value), ref pages);
					}
				}
				else
				{
					response.AddField("Error", "No results.");
				}

                if (pages.Count > 0)
                {
                    // Paginated
                    pages.Add(new Page(embed: response));
                    await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                }
                else
                {
                    await ctx.RespondAsync(response);
                }
            });

			await Task.FromResult(0);
		}

		[Command("findshiprepair"), Aliases("findshiprep")]
		[DescriptionAttribute("Find ships that are in need of repair given a condition threshold and a provided groupid")]
		public async Task FindShipRepair(CommandContext ctx,
			[DescriptionAttribute("Condition Threshold")]
			double conditionThreshold,
			[RemainingText]
			[DescriptionAttribute("Optional groupid.")]
			string remainingText)
		{
			if (conditionThreshold > 1.0)
			{
				// treat it as a percentage
				conditionThreshold /= 100;
			}

			_ = Task.Run(async () =>
			{
				await ctx.TriggerTypingAsync();

				ulong? groupId = null;
				if (!String.IsNullOrWhiteSpace(remainingText))
				{
					if (ulong.TryParse(remainingText, out ulong parsedGroupId))
					{
						groupId = parsedGroupId;
					}
				}
				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
				if (groupId == null)
				{
					response.AddField("Error", "GroupId not specified or set.");
					await ctx.RespondAsync(response);
					return;
				}

				var res = await GetGroupHubPayload(ctx, (ulong)groupId);
				var ErrorMessage = res.Item1;
				if (ErrorMessage != null)
				{
					response.AddField("Error", ErrorMessage);
					await ctx.RespondAsync(response);
					return;
				}

				var groupHubPayload = res.Item2;
				response.WithColor(new DiscordColor(79, 138, 16));

				if (groupHubPayload.Failures.Count > 0)
				{
					response.AddField("Errors:", string.Join("\n", groupHubPayload.Failures));
				}

				var playerShipsInFlight = groupHubPayload.PlayerShipsInFlight;
				var stationaryPlayerShips = groupHubPayload.PlayerModels
					.SelectMany(pm => pm.Locations)
					.SelectMany(l => l.StationaryPlayerShips)
					.ToList();
				var allPlayerShips = playerShipsInFlight.Union(stationaryPlayerShips);
				var shipsNeedingRepair = allPlayerShips
					.Where(ps => ps.Condition <= conditionThreshold)
					.GroupBy(ps => ps.ShipDisplayName)
					.Select(g => g.First())
					.ToList();

				var shipToResult = new SortedDictionary<string, List<string>>();
				foreach (var shipNeedingRepair in shipsNeedingRepair)
				{
					var repairParts = shipNeedingRepair.RepairMaterials
						.Select(rm => $"{rm.Amount}x{rm.MaterialTicker}")
						.ToList();
					var repairPartsStr = string.Join(", ", repairParts);
					if (!shipToResult.ContainsKey(shipNeedingRepair.PlayerName))
					{
						shipToResult.Add(shipNeedingRepair.PlayerName, new List<string>());
					}

					shipToResult[shipNeedingRepair.PlayerName].Add($"**{shipNeedingRepair.ShipDisplayName}**: {repairPartsStr} ({shipNeedingRepair.Condition * 100:N2}%)");
				}

				var pages = new List<Page>();
				if (shipToResult.Count > 0)
				{
					foreach (var kvp in shipToResult)
					{
						response.FIOAddField(kvp.Key, string.Join("\n", kvp.Value), ref pages);
					}
				}
				else
				{
					response.AddField("Error", "No results.");
				}

                if (pages.Count > 0)
                {
                    // Paginated
                    pages.Add(new Page(embed: response));
                    await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                }
                else
                {
                    await ctx.RespondAsync(response);
                }
            });

			await Task.FromResult(0);
		}

		[Command("findcurrency"), Aliases("findmoney", "findcurrencies", "findmonies")]
		[DescriptionAttribute("Find currencies for the given users and a provided groupid")]
		public async Task FindCurrency(CommandContext ctx,
            [DescriptionAttribute("The currency to look for")]
			string CurrencyToSearch,
			[RemainingText]
			[DescriptionAttribute("Usernames and optional groupid")]
			string remainingText)
		{
			_ = Task.Run(async () =>
			{
				await ctx.TriggerTypingAsync();

				List<string> filters = null;
				ulong? groupId = null;
				ParseRemainingText(remainingText, ref filters, ref groupId);
				groupId ??= await GetDefaultGroupId(ctx.Channel.Id);

				var response = new DiscordEmbedBuilder().WithColor(new DiscordColor(216, 0, 12));
				if (groupId == null)
				{
					response.AddField("Error", "GroupId not specified or set.");
					await ctx.RespondAsync(response);
					return;
				}

				var res = await GetGroupHubPayload(ctx, (ulong)groupId);
				var ErrorMessage = res.Item1;
				if (ErrorMessage != null)
				{
					response.AddField("Error", ErrorMessage);
					await ctx.RespondAsync(response);
					return;
				}

				var groupHubPayload = res.Item2;
				response.WithColor(new DiscordColor(79, 138, 16));

				if (groupHubPayload.Failures.Count > 0)
				{
					response.AddField("Errors:", string.Join("\n", groupHubPayload.Failures));
				}

				var playerModels = groupHubPayload.PlayerModels.OrderBy(pm => pm.UserName).ToList();
				ApplyUserNameFilters(ref playerModels, filters);

				var UserToAmmount = new Dictionary<string, double>();
				foreach (var playerModel in playerModels)
				{
					foreach (var currency in playerModel.Currencies.Where(c => c.Amount > 0.0 && c.CurrencyName.ToUpper() == CurrencyToSearch.ToUpper()))
					{
						UserToAmmount.Add(playerModel.UserName, currency.Amount);
					}
				}

				var pages = new List<Page>();
				if (UserToAmmount.Count > 0)
                {
					var CurrencyAmountsStr = string.Join("\n", UserToAmmount.Select(cua => $"`{cua.Key}`: {cua.Value:N2}").ToList());
					response.FIOAddField(CurrencyToSearch.ToUpper(), CurrencyAmountsStr, ref pages);
                }
				else
                {
					response.AddField("No results", "No results found.");
                }

                if (pages.Count > 0)
                {
                    // Paginated
                    pages.Add(new Page(embed: response));
                    await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(2));
                }
                else
                {
                    await ctx.RespondAsync(response);
                }
            });

			await Task.FromResult(0);
		}
	}
}