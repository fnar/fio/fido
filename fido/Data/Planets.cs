﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

using FIOWeb.JsonPayloads;
using Serilog;

namespace Fido.Data
{
    public class Planets
    {
        public static TimeSpan cacheExpireTime = TimeSpan.FromDays(1);
        public static MemoryCache planetCache = new MemoryCache(new MemoryCacheOptions());

        static Planets()
        {
            
        }

        private static async Task<List<PlanetDataPayload> > GetPlanetList()
        {
            List<PlanetDataPayload> list = planetCache.Get<List<PlanetDataPayload> >("planetlist");
            if (list == null)
            {
                Log.Information("Reaching out to server for Planet Data");
                Web.Request planetRequest = new Web.Request(HttpMethod.Get, "/planet/allplanets/full");
                list = await planetRequest.GetResponseAsync<List<PlanetDataPayload>>();
                planetCache.Set<List<PlanetDataPayload> >("planetlist", list, cacheExpireTime);
            }
            return list;
        }

        public static string EnvHighLow(double fieldvalue, double low, double high)
        {
            if (fieldvalue < low)
                return "Low";
            if (fieldvalue > high)
                return "High";
            return "Normal";
        }

        public static string FertilityStr(PlanetDataPayload planet)
        {
            if (planet.Fertility == -1.00)
                return "N/A";

            double fertility = planet.Fertility / 3.3;
            return (fertility > 0.0) ? $"+{fertility:P2}" : $"{fertility:P2}";
        }

        public static async Task<PlanetDataPayload> Find(String name_or_id)
        {
            List<PlanetDataPayload> planetList = await GetPlanetList();
            if (planetList == null)
            {
                Log.Error("Failed to get a planet list.\n");
                return null;
            }
            var planet = planetList.Find(x => x.PlanetNaturalId.ToUpper().Equals(name_or_id.ToUpper()));
            if(planet != null)
            {
                return planet;
            }
            return planetList.Find(x => x.PlanetName.ToUpper().Equals(name_or_id.ToUpper() ));
        }

        public static async Task<List<PlanetDataPayload>> FindPartial(String partial)
        {
            List<PlanetDataPayload> planetList = await GetPlanetList();
            if (planetList == null)
            {
                Log.Error("Failed to get a planet list.\n");
                return null;
            }
            return planetList.FindAll(x =>
                x.PlanetNaturalId.ToUpper().Contains(partial.ToUpper()) ||
                x.PlanetName.ToUpper().Contains(partial.ToUpper())
                );
        }

        private static BuildingCost GetBuildReqFromMaterial(MaterialPayload mat, int quantity)
        {
            BuildingCost bc = new BuildingCost();
            bc.Amount = quantity;
            bc.CommodityName = mat.Name;
            bc.CommodityTicker = mat.Ticker;
            bc.Volume = mat.Volume;
            bc.Weight = mat.Weight;
            return bc;
        }
        public static async Task<List<BuildingCost>> PlanetBuildRequirements(PlanetDataPayload planet, BuildingPayload building)
        {
            List<BuildingCost> requirements = new List<BuildingCost>();
            if (building.Name.StartsWith("planetaryProject") || building.Name.StartsWith("corporationProject"))
            {
                // planetary & corp projects are exempt from additional costs.
                return requirements;
            }

            if (planet.Surface == true)
            {
                MaterialPayload mat = await Materials.FindByTicker("MCG");
                requirements.Add(GetBuildReqFromMaterial(mat, 4 * building.AreaCost));
            }
            else
            {
                MaterialPayload mat = await Materials.FindByTicker("AEF");
                requirements.Add(GetBuildReqFromMaterial(mat, (int)Math.Ceiling(building.AreaCost / 3.0)));
            }
            if(planet.Pressure < 0.25)
            {
                MaterialPayload mat = await Materials.FindByTicker("SEA");
                requirements.Add(GetBuildReqFromMaterial(mat, building.AreaCost * 1));
            }
            if (planet.Pressure > 2.0)
            {
                MaterialPayload mat = await Materials.FindByTicker("HSE");
                requirements.Add(GetBuildReqFromMaterial(mat, 1));
            }
            if (planet.Gravity < 0.25)
            {
                MaterialPayload mat = await Materials.FindByTicker("MGC");
                requirements.Add(GetBuildReqFromMaterial(mat, 1));
            }
            if (planet.Gravity > 2.5)
            {
                MaterialPayload mat = await Materials.FindByTicker("BL");
                requirements.Add(GetBuildReqFromMaterial(mat, 1));
            }
            if (planet.Temperature < -25)
            {
                MaterialPayload mat = await Materials.FindByTicker("INS");
                requirements.Add(GetBuildReqFromMaterial(mat, building.AreaCost * 10));
            }
            if (planet.Temperature > 75)
            {
                MaterialPayload mat = await Materials.FindByTicker("TSH");
                requirements.Add(GetBuildReqFromMaterial(mat, 1));
            }
            return requirements;
        }

        public static double ResFactor(Resource res)
        {
            const string GaseousTypeStr = "GASEOUS";
            if (res.ResourceType.ToUpper() == GaseousTypeStr)
            {
                return 60.0;
            }

            return 70.0;
        }
    }
}
