﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

using Fido.Web;
using FIOWeb.JsonPayloads;
using Serilog;

namespace Fido.Data
{
    public class Systems
    {
        public static TimeSpan cacheExpireTime = TimeSpan.FromDays(30);
        public static MemoryCache cache = new MemoryCache(new MemoryCacheOptions());

        static Systems()
        {

        }

        private static async Task<List<SystemStarPayload>> GetSystemStars()
        {
            var list = cache.Get<List<SystemStarPayload>>("systemstars");
            if (list == null)
            {
                Log.Information("reaching out to server for SystemStars Data");
                Request request = new Request(HttpMethod.Get, "/systemstars");
                list = await request.GetResponseAsync<List<SystemStarPayload>>();
                cache.Set<List<SystemStarPayload>>("systemstars", list, cacheExpireTime);
            }

            return list;
        }

        public static async Task<SystemStarPayload> FindSystem(string system)
        {
            system = system.ToUpper();

            var list = await GetSystemStars();
            if (list == null)
            {
                Log.Error("Failed to get system stars from '/systemstars'.\n");
                return null;
            }

            return list.FirstOrDefault(s => s.Name.ToUpper().Contains(system) || s.NaturalId.ToUpper().Contains(system));
        }
    }
}
