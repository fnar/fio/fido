﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

using fido.JsonPayloads;
using FIOWeb.JsonPayloads;
using Serilog;

namespace Fido.Data
{
    public class Buildings
    {
        public static TimeSpan cacheExpireTime = TimeSpan.FromDays(30);
        public static MemoryCache buildingCache = new MemoryCache(new MemoryCacheOptions());

        static Buildings()
        {

        }

        private static async Task<List<BuildingPayload>> GetBuildingList()
        {
            List<BuildingPayload> list = buildingCache.Get<List<BuildingPayload>>("buildinglist");
            if (list == null)
            {
                Log.Information("Reaching out to server for Building Data");
                Web.Request request = new Web.Request(HttpMethod.Get, "/building/allbuildings");
                list = await request.GetResponseAsync<List<BuildingPayload>>();
                buildingCache.Set("buildinglist", list, cacheExpireTime);
            }
            return list;
        }

        private static async Task<List<WorkforceNeeds>> GetAllWorkforceNeeds()
        {
            List<WorkforceNeeds> list = buildingCache.Get<List<WorkforceNeeds>>("workforceneeds");
            if (list == null)
            {
                Log.Information("Reaching out to server for Workforce Needs Data");
                Web.Request request = new Web.Request(HttpMethod.Get, "/global/workforceneeds");
                list = await request.GetResponseAsync<List<WorkforceNeeds>>();
                buildingCache.Set("workforceneeds", list, cacheExpireTime);
            }
            return list;
        }

        public static async Task<List<BuildingPayload>> FindByName(String input)
        {
            List<BuildingPayload> buildingList = await GetBuildingList();
            if (buildingList == null)
            {
                Log.Error("Failed to get a building list from `/building/allbuildings`.\n");
                return null;
            }
            List<BuildingPayload> buildings = buildingList.FindAll(x => x.Name.ToUpper().Contains(input.ToUpper()));
            return buildings;
        }

        public static async Task<BuildingPayload> FindByTicker(String ticker)
        {
            List<BuildingPayload> buildingList = await GetBuildingList();
            if (buildingList == null)
            {
                Log.Error("Failed to get a building list from `/building/allbuildings`.\n");
                return null;
            }
            BuildingPayload building = buildingList.Find(x => x.Ticker.ToUpper().Equals(ticker.ToUpper()));
            return building;
        }

        public static async Task<List<BuildingPayload>> FindByInput(string ticker)
        {
            ticker = ticker.ToUpper();

            List<BuildingPayload> buildingList = await GetBuildingList();
            if (buildingList == null)
            {
                Log.Error("Failed to get a building list from `/building/allbuildings`.\n");
                return null;
            }

            return buildingList.Where(b => b.Recipes.Any(r => r.Inputs.Any(i => i.CommodityTicker.ToUpper() == ticker))).ToList();
        }

        public static async Task<List<BuildingPayload>> FindByOutput(string ticker)
        {
            ticker = ticker.ToUpper();

            List<BuildingPayload> buildingList = await GetBuildingList();
            if (buildingList == null)
            {
                Log.Error("Failed to get a building list from `/building/allbuildings`.\n");
                return null;
            }

            return buildingList.Where(b => b.Recipes.Any(r => r.Outputs.Any(i => i.CommodityTicker.ToUpper() == ticker))).ToList();
        }

        public static async Task<WorkforceNeeds> FindWorkforceNeedsByName(string workforceName)
        {
            workforceName = workforceName.ToUpper();

            List<WorkforceNeeds> workforceNeeds = await GetAllWorkforceNeeds();
            if (workforceNeeds == null)
            {
                Log.Error("Failed to get workforce needs from `/global/workforceneeds`.\n");
                return null;
            }
            WorkforceNeeds needs = workforceNeeds.FirstOrDefault(wn => wn.WorkforceType == workforceName);
            if (needs == null)
            {
                Log.Error("Provided workforce name is not valid.");
                return null;
            }

            return needs;
        }
    }
}
