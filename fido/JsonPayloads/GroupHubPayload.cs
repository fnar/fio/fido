﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace fido.JsonPayloads
{
    // The main model of the group hub
    public class GroupHubPayload : ICloneable
    {
        public string GroupName { get; set; }

        public List<GroupCXWarehouse> CXWarehouses { get; set; } = new List<GroupCXWarehouse>();

        public List<PlayerShip> PlayerShipsInFlight { get; set; } = new List<PlayerShip>();

        public List<PlayerModel> PlayerModels { get; set; } = new List<PlayerModel>();

        public List<string> Failures { get; set; } = new List<string>();

        public object Clone()
        {
            var newGroupHubPayload = new GroupHubPayload();
            newGroupHubPayload.GroupName = GroupName;
            CXWarehouses.ForEach((item) => { newGroupHubPayload.CXWarehouses.Add((GroupCXWarehouse)item.Clone()); });
            PlayerShipsInFlight.ForEach((item) => { newGroupHubPayload.PlayerShipsInFlight.Add((PlayerShip)item.Clone()); });
            PlayerModels.ForEach((item) => { newGroupHubPayload.PlayerModels.Add((PlayerModel)item.Clone()); });
            newGroupHubPayload.Failures = new List<string>(Failures);
            return newGroupHubPayload;
        }
    }

    // The model of the group views of the CX warehouses
    public class GroupCXWarehouse : ICloneable
    {
        public string WarehouseLocationName { get; set; }
        public string WarehouseLocationNaturalId { get; set; }

        public List<PlayerStorage> PlayerCXWarehouses { get; set; } = new List<PlayerStorage>();

        public object Clone()
        {
            var newGroupCXWarehouse = new GroupCXWarehouse();
            newGroupCXWarehouse.WarehouseLocationName = WarehouseLocationName;
            newGroupCXWarehouse.WarehouseLocationNaturalId = WarehouseLocationNaturalId;
            PlayerCXWarehouses.ForEach((item) => { newGroupCXWarehouse.PlayerCXWarehouses.Add((PlayerStorage)item.Clone()); });
            return newGroupCXWarehouse;
        }
    }

    // A model of an individual storage location for a player
    public class PlayerStorage : ICloneable
    {
        public string PlayerName { get; set; }

        public string StorageType { get; set; }

        public List<GroupHubInventoryItem> Items { get; set; } = new List<GroupHubInventoryItem>();

        public DateTime LastUpdated { get; set; }

        public object Clone()
        {
            var newPlayerStorage = new PlayerStorage();
            newPlayerStorage.PlayerName = PlayerName;
            newPlayerStorage.StorageType = StorageType;
            Items.ForEach((item) => { newPlayerStorage.Items.Add((GroupHubInventoryItem)item.Clone()); });
            newPlayerStorage.LastUpdated = LastUpdated;
            return newPlayerStorage;
        }
    }

    // The model of an individual player
    public class PlayerModel : ICloneable
    {
        public string UserName { get; set; }

        public List<PlayerCurrency> Currencies { get; set; } = new List<PlayerCurrency>();

        public List<PlayerLocation> Locations { get; set; } = new List<PlayerLocation>();

        public object Clone()
        {
            var newPlayerModel = new PlayerModel();
            newPlayerModel.UserName = UserName;
            Currencies.ForEach((item) => { newPlayerModel.Currencies.Add((PlayerCurrency)item.Clone()); });
            Locations.ForEach((item) => { newPlayerModel.Locations.Add((PlayerLocation)item.Clone()); });
            return newPlayerModel;
        }
    }

    // The model of one of a player's currencies (CIS, AIC, etc...)
    public class PlayerCurrency : ICloneable
    {
        public string CurrencyName { get; set; }

        public double Amount { get; set; }

        public DateTime LastUpdated { get; set; }

        public object Clone()
        {
            var newPlayerCurrency = new PlayerCurrency();
            newPlayerCurrency.CurrencyName = CurrencyName;
            newPlayerCurrency.Amount = Amount;
            newPlayerCurrency.LastUpdated = LastUpdated;
            return newPlayerCurrency;
        }
    }

    // The model of an individual location a player has some presense in
    public class PlayerLocation : ICloneable
    {
        public string LocationIdentifier { get; set; }
        public string LocationName { get; set; }

        [JsonIgnore]
        public string LocationDisplayName
        {
            get
            {
                if (LocationName != null && LocationName != LocationIdentifier)
                {
                    return $"{LocationName} ({LocationIdentifier})";
                }

                return LocationIdentifier;
            }
        }

        public List<PlayerBuilding> Buildings { get; set; } = new List<PlayerBuilding>();

        public List<PlayerProductionLine> ProductionLines { get; set; } = new List<PlayerProductionLine>();

        public PlayerStorage BaseStorage { get; set; } = new PlayerStorage();

        public PlayerStorage WarehouseStorage { get; set; } = new PlayerStorage();

        public List<PlayerShip> StationaryPlayerShips { get; set; } = new List<PlayerShip>();

        public object Clone()
        {
            var newPlayerLocation = new PlayerLocation();
            newPlayerLocation.LocationIdentifier = LocationIdentifier;
            newPlayerLocation.LocationName = LocationName;
            Buildings.ForEach((item) => { newPlayerLocation.Buildings.Add((PlayerBuilding)item.Clone()); });
            ProductionLines.ForEach((item) => { newPlayerLocation.ProductionLines.Add((PlayerProductionLine)item.Clone()); });
            newPlayerLocation.BaseStorage = ((BaseStorage != null) ? (PlayerStorage)BaseStorage.Clone() : null);
            newPlayerLocation.WarehouseStorage = ((WarehouseStorage != null) ? (PlayerStorage)WarehouseStorage.Clone() : null);
            StationaryPlayerShips.ForEach((item) => { newPlayerLocation.StationaryPlayerShips.Add((PlayerShip)item.Clone()); });

            return newPlayerLocation;
        }
    }

    public class PlayerBuilding : ICloneable
    {
        public string BuildingName { get; set; }
        public string BuildingTicker { get; set; }

        public double Condition { get; set; }
        public List<GroupHubInventoryItem> RepairMaterials { get; set; } = new List<GroupHubInventoryItem>();

        public object Clone()
        {
            var newPlayerBuilding = new PlayerBuilding();
            newPlayerBuilding.BuildingName = BuildingName;
            newPlayerBuilding.BuildingTicker = BuildingTicker;
            newPlayerBuilding.Condition = Condition;
            RepairMaterials.ForEach((item) => { newPlayerBuilding.RepairMaterials.Add((GroupHubInventoryItem)item.Clone()); });

            return newPlayerBuilding;
        }
    }

    public class PlayerProductionLine : ICloneable
    {
        public string BuildingName { get; set; }
        public string BuildingTicker { get; set; }

        public int Capacity { get; set; }
        public double Efficiency { get; set; }
        public double Condition { get; set; }

        public List<PlayerProductionOrder> ProductionOrders { get; set; } = new List<PlayerProductionOrder>();

        public object Clone()
        {
            var newPlayerProductionLine = new PlayerProductionLine();
            newPlayerProductionLine.BuildingName = BuildingName;
            newPlayerProductionLine.BuildingTicker = BuildingTicker;
            newPlayerProductionLine.Capacity = Capacity;
            newPlayerProductionLine.Efficiency = Efficiency;
            newPlayerProductionLine.Condition = Condition;
            ProductionOrders.ForEach((item) => { newPlayerProductionLine.ProductionOrders.Add((PlayerProductionOrder)item.Clone()); });

            return newPlayerProductionLine;
        }
    }

    // The model of an individual production order in progress at a PlayerLocation
    public class PlayerProductionOrder : ICloneable
    {
        public bool Started { get; set; }
        public bool Halted { get; set; }
        public bool Recurring { get; set; }

        public TimeSpan OrderDuration { get; set; }
        public DateTime? TimeCompletion { get; set; }

        public List<GroupHubInventoryItem> Inputs { get; set; } = new List<GroupHubInventoryItem>();
        public List<GroupHubInventoryItem> Outputs { get; set; } = new List<GroupHubInventoryItem>();

        public DateTime LastUpdated { get; set; }

        public object Clone()
        {
            var newPlayerProductionOrder = new PlayerProductionOrder();
            newPlayerProductionOrder.Started = Started;
            newPlayerProductionOrder.Halted = Halted;
            newPlayerProductionOrder.Recurring = Recurring;
            newPlayerProductionOrder.OrderDuration = OrderDuration;
            newPlayerProductionOrder.TimeCompletion = TimeCompletion;
            Inputs.ForEach((item) => { newPlayerProductionOrder.Inputs.Add((GroupHubInventoryItem)item.Clone()); });
            Outputs.ForEach((item) => { newPlayerProductionOrder.Outputs.Add((GroupHubInventoryItem)item.Clone()); });
            newPlayerProductionOrder.LastUpdated = LastUpdated;

            return newPlayerProductionOrder;
        }
    }

    // A model of an individual inventory item of a player.
    public class GroupHubInventoryItem : ICloneable
    {
        public string MaterialTicker { get; set; }
        public string MaterialName { get; set; }
        public string MaterialCategoryName { get; set; }

        [JsonIgnore]
        public string MaterialType
        {
            get
            {
                switch (MaterialCategoryName)
                {
                    case "consumables (basic)":
                        return "consumables_basic";
                    case "consumables (luxury)":
                        return "consumables_luxury";
                    default:
                        if (MaterialCategoryName == null)
                        {
                            return "";
                        }

                        return MaterialCategoryName.Replace(" ", "_");
                }
            }
        }

        public int Units { get; set; }

        public object Clone()
        {
            var newGroupHubInventoryItem = new GroupHubInventoryItem();
            newGroupHubInventoryItem.MaterialTicker = MaterialTicker;
            newGroupHubInventoryItem.MaterialName = MaterialName;
            newGroupHubInventoryItem.MaterialCategoryName = MaterialCategoryName;
            newGroupHubInventoryItem.Units = Units;

            return newGroupHubInventoryItem;
        }
    }

    // A model of one of a player's ships
    public class PlayerShip : ICloneable
    {
        public string PlayerName { get; set; }

        public string ShipName { get; set; }
        public string ShipRegistration { get; set; }

        [JsonIgnore]
        public string ShipDisplayName
        {
            get
            {
                return (ShipName != null) ? ShipName : ShipRegistration;
            }
        }

        public string Location { get; set; }
        public string Destination { get; set; }

        public string DisplayLocation
        {
            get
            {
                if (!String.IsNullOrEmpty(Destination))
                {
                    return $"-> {Destination}";
                }

                return Location;
            }
        }

        public DateTime? LocationETA { get; set; }

        public DateTime? LocationETALocalTime
        {
            get
            {
                if (LocationETA != null)
                {
                    return ((DateTime)LocationETA).ToLocalTime();
                }

                return null;
            }
        }

        public Fuel Fuel { get; set; } = new Fuel();

        public double Condition { get; set; }
        public List<ShipRepairMat> RepairMaterials { get; set; }

        public PlayerStorage Cargo { get; set; } = new PlayerStorage();

        public DateTime LastUpdated { get; set; }

        public object Clone()
        {
            var newPlayerShip = new PlayerShip();
            newPlayerShip.PlayerName = PlayerName;
            newPlayerShip.ShipName = ShipName;
            newPlayerShip.ShipRegistration = ShipRegistration;
            newPlayerShip.Location = Location;
            newPlayerShip.Destination = Destination;
            newPlayerShip.LocationETA = LocationETA;
            newPlayerShip.Fuel = ((Fuel != null) ? (Fuel)Fuel.Clone() : null);
            newPlayerShip.Condition = Condition;

            newPlayerShip.RepairMaterials = new List<ShipRepairMat>();
            foreach (var repairMat in RepairMaterials)
            {
                newPlayerShip.RepairMaterials.Add((ShipRepairMat)repairMat.Clone());
            }

            newPlayerShip.Cargo = ((Cargo != null) ? (PlayerStorage)Cargo.Clone() : null);
            newPlayerShip.LastUpdated = LastUpdated;

            return newPlayerShip;
        }
    }

    // A representation of a ship's fuel.
    public class Fuel : ICloneable
    {
        public int CurrentSF { get; set; }
        public int MaxSF { get; set; }

        public int CurrentFF { get; set; }
        public int MaxFF { get; set; }

        public object Clone()
        {
            var newFuel = new Fuel();
            newFuel.CurrentSF = CurrentSF;
            newFuel.MaxSF = MaxSF;
            newFuel.CurrentFF = CurrentFF;
            newFuel.MaxFF = MaxFF;

            return newFuel;
        }
    }

    public class ShipRepairMat : ICloneable
    {
        public string MaterialTicker { get; set; }
        public int Amount { get; set; }

        public object Clone()
        {
            var newRepairMat = new ShipRepairMat();
            newRepairMat.MaterialTicker = MaterialTicker;
            newRepairMat.Amount = Amount;

            return newRepairMat;
        }
    }

    internal static class Extension
    {
        public static string ToUpdatedAgo(this DateTime dt)
        {
            TimeSpan ts = DateTime.UtcNow - dt;
            if (ts.TotalDays > 1.0)
            {
                return $"{ts.TotalDays.ToString("N1")}d ago";
            }
            else if (ts.TotalHours >= 1.0)
            {
                return $"{ts.TotalHours.ToString("N1")}h ago";
            }

            return "<1h ago";
        }

        public static string ToCompletionTime(this DateTime? dt)
        {
            if (dt != null)
            {
                if (DateTime.UtcNow > (DateTime)dt)
                {
                    return "Completed";
                }
                else
                {
                    TimeSpan ts = (DateTime)dt - DateTime.UtcNow;
                    return $"{ts.Hours}h {ts.Minutes}m remaining";
                }
            }

            return String.Empty;
        }
    }
}