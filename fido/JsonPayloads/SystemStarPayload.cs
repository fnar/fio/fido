using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIOWeb.JsonPayloads
{
    public class SystemStarPayload
    {
        public string SystemId { get; set; }
        public string Name { get; set; }
        public string NaturalId { get; set; }
        public string Type { get; set; }
        public float PositionX { get; set; }
        public float PositionY { get; set; }
        public float PositionZ { get; set; }
        public string SectorId { get; set; }
        public string SubSectorId { get; set; }
        public List<SystemConnection> Connections { get; set; } = new List<SystemConnection>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        [JsonIgnore]
        public string DisplayName
        {
            get
            {
                if (NaturalId != Name)
                {
                    return $"{Name} ({NaturalId})";
                }
                else
                {
                    return NaturalId;
                }
            }
        }
    }

    public class SystemConnection
    {
        public string SystemConnectionId { get; set; }
        public string ConnectingId { get; set; }
    }
}
