﻿using System.Collections.Generic;

namespace fido.JsonPayloads
{
	public class WorkforceNeeds
	{
		public List<Need> Needs { get; set; }
		public string WorkforceType { get; set; }
	}

	public class Need
	{
		public string MaterialId { get; set; }
		public string MaterialName { get; set; }
		public string MaterialTicker { get; set; }
		public string MaterialCategory { get; set; }
		public float Amount { get; set; }
	}

}
