﻿namespace fido.JsonPayloads
{
    public class JumpRoutePayload
    {
        public string SourceSystemId { get; set; }
        public string SourceSystemName { get; set; }
        public string SourceSystemNaturalId { get; set; }

        public string DestinationSystemId { get; set; }
        public string DestinationSystemName { get; set; }
        public string DestinationNaturalId { get; set; }

        public double Distance { get; set; }
    }
}
