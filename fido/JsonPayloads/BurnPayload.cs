﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace fido.JsonPayloads
{
    public class BurnPayload : ICloneable
    {
        public string UserName { get; set; }
        public string Error { get; set; }

        public string PlanetId { get; set; }
        public string PlanetName { get; set; }
        public string PlanetNaturalId { get; set; }

        [JsonIgnore]
        public string PlanetDisplayName
        {
            get
            {
                if (PlanetName != PlanetNaturalId)
                {
                    return $"{PlanetName} ({PlanetNaturalId})";
                }

                return PlanetNaturalId;
            }
        }

        public DateTime? PlanetConsumptionTime { get; set; }

        public DateTime LastUpdate { get; set; } = DateTime.MinValue;
        public string LastUpdateCause { get; set; }

        public List<BurnInventoryItem> Inventory { get; set; } = new List<BurnInventoryItem>();

        public List<BurnConsumptionProduction> WorkforceConsumption { get; set; } = new List<BurnConsumptionProduction>();
        public List<BurnConsumptionProduction> OrderConsumption { get; set; } = new List<BurnConsumptionProduction>();
        public List<BurnConsumptionProduction> OrderProduction { get; set; } = new List<BurnConsumptionProduction>();

        public object Clone()
        {
            var newBurnPayload = new BurnPayload();
            newBurnPayload.UserName = UserName;
            newBurnPayload.Error = Error;
            newBurnPayload.PlanetId = PlanetId;
            newBurnPayload.PlanetName = PlanetName;
            newBurnPayload.PlanetNaturalId = PlanetNaturalId;
            newBurnPayload.PlanetConsumptionTime = PlanetConsumptionTime;
            newBurnPayload.LastUpdate = LastUpdate;
            newBurnPayload.LastUpdateCause = LastUpdateCause;
            Inventory.ForEach((item) => { newBurnPayload.Inventory.Add((BurnInventoryItem)item.Clone()); });
            WorkforceConsumption.ForEach((item) => { newBurnPayload.WorkforceConsumption.Add((BurnConsumptionProduction)item.Clone()); });
            OrderConsumption.ForEach((item) => { newBurnPayload.OrderConsumption.Add((BurnConsumptionProduction)item.Clone()); });
            OrderProduction.ForEach((item) => { newBurnPayload.OrderProduction.Add((BurnConsumptionProduction)item.Clone()); });
            return newBurnPayload;
        }
    }

    public class BurnInventoryItem : ICloneable
    {
        public string MaterialId { get; set; }
        public string MaterialTicker { get; set; }
        public int MaterialAmount { get; set; }

        public object Clone()
        {
            var newBurnInventoryItem = new BurnInventoryItem();
            newBurnInventoryItem.MaterialId = MaterialId;
            newBurnInventoryItem.MaterialTicker = MaterialTicker;
            newBurnInventoryItem.MaterialAmount = MaterialAmount;
            return newBurnInventoryItem;
        }
    }

    public class BurnConsumptionProduction : ICloneable
    {
        public string MaterialId { get; set; }
        public string MaterialTicker { get; set; }
        public double DailyAmount { get; set; } = 0.0;

        public object Clone()
        {
            var newBurnConsumptionProduction = new BurnConsumptionProduction();
            newBurnConsumptionProduction.MaterialId = MaterialId;
            newBurnConsumptionProduction.MaterialTicker = MaterialTicker;
            newBurnConsumptionProduction.DailyAmount = DailyAmount;
            return newBurnConsumptionProduction;
        }
    }
}
