﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace fido.JsonPayloads
{
	public class GroupPayload
	{
		public int GroupModelId { get; set; }

		public string GroupOwner { get; set; }

		public string GroupName { get; set; }

		public virtual List<GroupUserEntry> GroupUsers { get; set; } = new List<GroupUserEntry>();

		[JsonIgnore]
		public IEnumerable<string> GroupUserNames
		{
			get
			{
				var res = GroupUsers.Select(gu => gu.GroupUserName).OrderBy(gu => gu).ToList();
				return res;
			}
		}
	}

	public class GroupUserEntry
	{
		public string GroupUserName { get; set; }

		public int GroupModelId { get; set; }
	}
}