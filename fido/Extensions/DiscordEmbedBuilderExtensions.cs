﻿using System;
using System.Collections.Generic;
using System.Linq;

using DSharpPlus.Entities;
using DSharpPlus.Interactivity;

namespace fido.Extensions
{
    public static class DiscordEmbedBuilderExtensions
    {
        public static DiscordEmbedBuilder FIOAddField(this DiscordEmbedBuilder deb, string key, string value, ref List<Page> pages)
        {
            const int FieldCountThreshold = 10;

            if (deb == null) throw new ArgumentNullException(nameof(deb));
            if (key == null || key.Length == 0 || key.Length > 256) throw new ArgumentNullException(nameof(key));
            if (value == null || value.Length == 0) throw new ArgumentNullException(nameof(value));
            if (pages == null) throw new ArgumentNullException(nameof(pages));

            // If value is larger than 1024 characters, we need to chunk it. The issue, though, is that we want to chunk it on
            // newlines NOT in the middle of a string.
            if (value.Length > 1024)
            {
                // Find the indices of all newlines
                var newlineIndices = new List<int>();
                for (var charIdx = 0; charIdx < value.Length; ++charIdx)
                {
                    if (value[charIdx] == '\n')
                    {
                        newlineIndices.Add(charIdx);
                    }
                }

                var fields = new List<Tuple<string, string>>();
                while(value.Length > 0)
                {
                    // Find the largest newline entry that is closest to 1024 but not exceeding it
                    var newlineIndex = newlineIndices
                        .Where(ni => ni < 1000)     // Only consider entries with an index < 1000 -- giving us some breathing room
                        .OrderBy(ni => ni)          // Ensure sorted
                        .Last();                    // Grab the last entry

                    // Grab the string from 0 to newlineIndex
                    var subValue = value[..newlineIndex];

                    // Set the remaining string to the tail end
                    value = value[newlineIndex..subValue.Length];

                    // Remove all the newlines we used and subtract newlineIndex from all remaining entries
                    newlineIndices = newlineIndices
                        .Where(ni => ni >= 1000)            // Grab the remaining indices
                        .Select(ni => ni - newlineIndex)    // Subtract newlineIndex from each those
                        .OrderBy(ni => ni)                  // Ensure sorted
                        .ToList();                          // ToList

                    fields.Add(new Tuple<string, string>(key, subValue));
                }

                // Now that we've populated fields, try adding said fields while we're less than 25 
                for (var fieldIndex = 0; fieldIndex < fields.Count; ++fieldIndex)
                {
                    var field = fields[fieldIndex];

                    if (deb.Fields.Count >= FieldCountThreshold)
                    {
                        // We need to throw this DEB into the Pages list and create a new DEB
                        pages.Add(new Page(embed: deb));
                        deb.ClearFields();
                        deb = new DiscordEmbedBuilder();
                    }

                    deb.AddField($"{field.Item1} ({fieldIndex + 1}/{fields.Count})", field.Item2);
                }
            }
            else
            {
                if (deb.Fields.Count >= FieldCountThreshold)
                {
                    pages.Add(new Page(embed: deb));
                    deb.ClearFields();
                    deb = new DiscordEmbedBuilder();
                }

                deb.AddField(key, value);
            }

            return deb;
        }
    }
}
