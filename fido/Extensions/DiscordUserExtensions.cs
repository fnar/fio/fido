﻿using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fido.Extensions
{
    public static class DiscordUserExtensions
    {
        public static string GetFullyQualifiedUsername(this DiscordUser user)
        {
            if (user.Discriminator == null || user.Discriminator == "0")
            {
                return user.Username;
            }

            return $"{user.Username}#{user.Discriminator}";
        }
    }
}
