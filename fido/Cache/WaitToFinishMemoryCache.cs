﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace Fido.Cache
{
    public class WaitToFinishMemoryCache<TItem>
    {
        private MemoryCache _cache = new MemoryCache(new MemoryCacheOptions());
        private ConcurrentDictionary<object, SemaphoreSlim> _locks = new ConcurrentDictionary<object, SemaphoreSlim>();

        public async Task<TItem> GetOrCreate(object key, Func<Task<TItem>> createItem, int cachetime = 600)
        {
            TItem cacheEntry;

            if (!_cache.TryGetValue(key, out cacheEntry))// Look for cache key.
            {
                SemaphoreSlim mylock = _locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));

                await mylock.WaitAsync();
                try
                {
                    if (!_cache.TryGetValue(key, out cacheEntry))
                    {
                        // Key not in cache, so get data.
                        cacheEntry = await createItem();
                        var cacheEntryOptions = GetCacheOptions(cachetime);
                        _cache.Set(key, cacheEntry, cacheEntryOptions);
                    }
                }
                finally
                {
                    mylock.Release();
                }
            }
            return cacheEntry;
        }

        private MemoryCacheEntryOptions GetCacheOptions(int cacheTime)
        {
            var options = new MemoryCacheEntryOptions()
                .SetSize(1);
            if(cacheTime > 0)
            {
                options.SetPriority(CacheItemPriority.Normal);
                options.SetSlidingExpiration(TimeSpan.FromSeconds(cacheTime));
            }
            else
            {
                options.SetPriority(CacheItemPriority.High);
            }
            return options;
        }

    }
}
