using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Fido.Cache;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using Tommy;
using Fido.Data;
using fido.Database;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;

namespace Fido
{
    public static class Globals
    {
        /* Yeah, sure globals are bad, and whatnot, but I want configuration
         * to be easy & straightforward. */
        public static TomlTable config { get; set; }
        public static WaitToFinishMemoryCache<object> cache { get; set; }
        public static TomlTable blame { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", true)
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            MainAsync().GetAwaiter().GetResult();

            Log.CloseAndFlush();
        }

        private static DiscordClient discord = null;

        static async Task MainAsync()
        {
            using (var db = new FIDODbContext())
            {
                Log.Information("Migrating Database");
                db.Database.Migrate();
            }

            //Globals.cache = new WaitToFinishMemoryCache<object>;
            try
            {
                using (StreamReader reader = new StreamReader(File.OpenRead("config.toml")))
                {
                    Globals.config = TOML.Parse(reader);
                }
            }
            catch
            {
                Console.Write("Problem reading config file.  Exiting.\n");
                System.Environment.Exit(1);
            }
            Web.WebConsts.RootUrl = Globals.config["fio"]["host"];

            discord = new DiscordClient(new DiscordConfiguration()
            {
                Token = Globals.config["discord"]["token"],
                TokenType = TokenType.Bot,
                Intents = DiscordIntents.AllUnprivileged | DiscordIntents.MessageContents
            });

            var prefix = Globals.config["discord"]["prefix"].AsString.Value;

            discord.UseInteractivity(new InteractivityConfiguration()
            {
                Timeout = TimeSpan.FromSeconds(60),
                PollBehaviour = PollBehaviour.KeepEmojis,
                ButtonBehavior = ButtonPaginationBehavior.DeleteButtons,
                PaginationBehaviour = PaginationBehaviour.Ignore,
                PaginationDeletion = PaginationDeletion.DeleteEmojis,
            });

            var commands = discord.UseCommandsNext(new CommandsNextConfiguration()
            {
                StringPrefixes = new[] { prefix },
                PrefixResolver = FIDOPrefixResolver,
                //EnableMentionPrefix = false
            });

            commands.RegisterCommands<commands.BuildingCommands>();
            commands.RegisterCommands<commands.ChatCommands>();
            commands.RegisterCommands<commands.FioCommands>();
            commands.RegisterCommands<commands.JumpCommands>();
            commands.RegisterCommands<commands.MaterialCommands>();
            commands.RegisterCommands<commands.PlanetCommands>();
            commands.RegisterCommands<commands.SpaceCommands>();

            discord.Ready += async (s, e) =>
            {
                await discord.UpdateStatusAsync(new DiscordActivity("with PrUn Data", ActivityType.Playing));
            };

            await discord.ConnectAsync();
            await msgchan(discord);
            await Task.Delay(-1);
        }

        private static List<string> AcceptablePrefixes = new List<string>()
        {
            "`",
            "~",
            "$",
            "?",
            "-",
            "#",
            "f.",
            "f:",
            "fido:"
        };

        public static async Task<int> FIDOPrefixResolver(DiscordMessage msg)
        {
            // Suppress no async call warning
            await Task.FromResult(0);

            foreach (var AcceptablePrefix in AcceptablePrefixes)
            {
                var res = msg.GetStringPrefixLength(AcceptablePrefix, StringComparison.InvariantCultureIgnoreCase);
                if (res != -1)
                {
                    return res;
                }
            }

            return -1;
        }

        static async Task msgchan(DiscordClient discord)
        {
            if (Globals.config["discord"].HasKey("debug_channel"))
            {
                string chanid_str = Globals.config["discord"]["debug_channel"];
                ulong debug_chanid = Convert.ToUInt64(chanid_str);
                var channel = await discord.GetChannelAsync(debug_chanid);
                await discord.SendMessageAsync(channel, "I'm alive!");
            }
            // Kick off caching of the big (static) queries
            // Buildings, planets?, etc.
            Fido.commands.ChatCommands.ReloadBlame();
#if !DEBUG
            await Planets.Find("Katoa");
#endif
            await Buildings.FindByTicker("rig");
            await Buildings.FindWorkforceNeedsByName("pioneer");
            await Materials.FindByTicker("dw");
            await Recipes.FindByOutputTicker("h20");
            await Exchanges.GetExchangeList();
            await Planets.Find("Katoa");
            await Systems.FindSystem("");
            Log.Information("Fully cached and ready.");
        }
    }
}
